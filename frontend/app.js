import './scss/index.scss';

import './assets/js/index.js';

import './assets/images/sf.png';
import './assets/images/bg.jpg';
import './assets/images/404.png';
import './assets/images/500.png';
import './assets/images/logo.png';
import './assets/images/profile.png';
import './assets/images/word.png';
import './assets/images/pdf.png';


import './assets/images/datatables/sort_asc.png';
import './assets/images/datatables/sort_asc_disabled.png';
import './assets/images/datatables/sort_both.png';
import './assets/images/datatables/sort_desc.png';
import './assets/images/datatables/sort_desc_disabled.png';

import './assets/fonts/icons/fontawesome/fontawesome-webfont.eot';
import './assets/fonts/icons/fontawesome/fontawesome-webfont.woff';
import './assets/fonts/icons/fontawesome/fontawesome-webfont.woff2';
import './assets/fonts/icons/fontawesome/fontawesome-webfont.svg';
import './assets/fonts/icons/fontawesome/fontawesome-webfont.ttf';

import './assets/fonts/icons/themify/themify.eot';
import './assets/fonts/icons/themify/themify.svg';
import './assets/fonts/icons/themify/themify.ttf';
import './assets/fonts/icons/themify/themify.woff';



const $ = require('jquery');
global.$ = global.jQuery = $;