# -*- text -*-
#
#  $Id: 4b7e4585c029b8617aa7b9169a42bf50a5ec4938 $

#
#  Lightweight Directory Access Protocol (LDAP)
#
ldap {
	server = $ENV{AD_HOST}
	#server = ""
	base_dn = $ENV{AD_BASE_DN}
	identity = $ENV{AD_BIND_USER}
	password = $ENV{AD_BIND_PASS}

	user {
	  base_dn = "${..base_dn}"
	  filter = "(sAMAccountName=%{%{Stripped-User-Name}:-%{User-Name}})"
	}

	group {
		base_dn = "${..base_dn}"
		membership_attribute = 'memberOf'
		membership_filter = "(&(objectcategory=group)(member:1.2.840.113556.1.4.1941:=%{control:Ldap-UserDn}))"
	}

	update {
		control:Password-With-Header	+= 'userPassword'
		control:LM-Password		:= 'lmPassword'
		control:NT-Password		:= 'ntPassword'
		reply:Reply-Message		:= 'radiusReplyMessage'
		reply:Tunnel-Type		:= 'radiusTunnelType'
		reply:Tunnel-Medium-Type	:= 'radiusTunnelMediumType'
		reply:Tunnel-Private-Group-ID	:= 'msRADIUS-FramedInterfaceId'

		#  Where only a list is specified as the RADIUS attribute,
		#  the value of the LDAP attribute is parsed as a valuepair
		#  in the same format as the 'valuepair_attribute' (above).
		control:			+= 'radiusControlAttribute'
		request:			+= 'radiusRequestAttribute'
		reply:				+= 'radiusReplyAttribute'
	}

	attribute {
	  ipaddr				= 'radiusClientIdentifier'
	  secret				= 'radiusClientSecret'
	}

	options {
	  chase_referrals = yes
	  rebind = yes
	}

	tls {
		require_cert = 'never'
	}
}