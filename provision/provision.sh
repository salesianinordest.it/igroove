#!/usr/bin/env bash


export DEBIAN_FRONTEND=noninteractive

if [ ! -f  /etc/apt/sources.list.d/php.list ]; then
    echo "Update packages"
    apt-get update
    apt-get install wget -y
    wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
    apt-get install ca-certificates apt-transport-https wget -y
    echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
    apt-get update
    apt-get upgrade -y
fi



if [ ! -f  /usr/bin/php ]; then
echo "Installing APT packages"
apt-get install lsb-release ca-certificates sudo wget curl git-core build-essential \
    vim elinks zip unzip software-properties-common apache2 \
    libapache2-mod-php7.3 php7.3-cli php7.3-curl php7.3-gd php7.3-mbstring  \
    php7.3-dev php7.3-mysql php7.3-xml php7.3-json php7.3-intl php7.3-zip php7.3-bcmath \
    php7.3-ldap php7.3-soap rabbitmq-server supervisor \
      autoconf automake build-essential -y
fi




#echo "Installing packages"
#apt install sudo wget curl git-core vim elinks apache2  php-common \
#        libapache2-mod-php  php-cli php-curl php-intl php-zip php-ldap \
#        php-gd php-mcrypt php-mysql php-xml php-json php-mbstring php-soap \
#        php-bcmath php-ldap rabbitmq-server libevent-dev supervisor  php-dev \
#        php-bcmath -y
rabbitmq-plugins enable rabbitmq_management rabbitmq_tracing  rabbitmq_federation


cp /vagrant/provision/linuxConfiguration/etc/rabbitmq/rabbitmq.config /etc/rabbitmq/rabbitmq.config




service rabbitmq-server restart
rabbitmqctl add_vhost /dev/
rabbitmqctl set_permissions -p /dev/ guest ".*" ".*" ".*"

if [ ! -f  /usr/local/bin/composer ]; then
    echo "Get composer"
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
    mv /usr/bin/composer.phar /usr/bin/composer
    chmod a+x /usr/bin/composer
fi

if [ "$(grep -c public /etc/apache2/sites-enabled/000-default.conf )"=="0" ]; then
    echo "Configuring Apache2"
    sed -i 's/\/var\/www\/html/\/vagrant\/public/g'  /etc/apache2/sites-enabled/000-default.conf
    sed -i 's/<\/VirtualHost>//g'  /etc/apache2/sites-enabled/000-default.conf
    cat <<EOF >> /etc/apache2/sites-enabled/000-default.conf
    <Directory /vagrant/public>
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
    a2enmod rewrite
    service apache2 restart
fi

if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
   sudo mysql -u root -e "update mysql.user set plugin = 'mysql_native_password' where User='root';"
    mysqladmin -u root password root
    service mysql restart
fi
if ls -c1 /var/lib/mysql | grep igroove; then
     echo "Database exists nothing to do"
else
     echo "Create database"
     mysqladmin -u root -proot create igroove
fi
if [ ! -f /vagrant/vendor ]; then
    echo "Get vendors via composer"
    cd /vagrant
    composer install 
fi



echo "Installing postfix"
    debconf-set-selections <<< "postfix postfix/mailname string sanzeno.org"
    debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
    apt-get install -y postfix


if [ ! -f  /etc/supervisor/conf.d/ldap_service.conf ]; then
    mkdir /var/log/supervisor
    cp -r /vagrant/provision/linuxConfiguration/etc/supervisor/conf.d/* /etc/supervisor/conf.d/
    service supervisor restart
    service supervisor status
    ln -s /vagrant /var/www/igroove
fi

if [ ! -f  /etc/ldap/certs/server.test.network.cer ]; then
    mkdir -p /etc/ldap/certs/
    cp /vagrant/provision/linuxConfiguration/etc/ldap/ldap.conf /etc/ldap/ldap.conf
    cp /vagrant/provision/linuxConfiguration/etc/ldap/certs/server.test.network.cer /etc/ldap/certs/server.test.network.cer
fi

if [ ! -f  /vagrant/src/Zen/IgrooveBundle/Resources/config/version/versionAvailable.yml ]; then
    cp /vagrant/src/Zen/IgrooveBundle/Resources/config/version/versionAvailable.yml.dist /vagrant/src/Zen/IgrooveBundle/Resources/config/version/versionAvailable.yml
    cp /vagrant/src/Zen/IgrooveBundle/Resources/config/version/versionInstalled.yml.dist /vagrant/src/Zen/IgrooveBundle/Resources/config/version/versionInstalled.yml
fi






if [ ! -f /usr/lib/php/20180731/xdebug.so ]; then
    echo "Configuring php xdebug"
    wget https://xdebug.org/files/xdebug-2.7.0RC1.tgz -O /tmp/xdebug-2.7.0RC1.tgz
    cd /tmp
    tar -xvzf xdebug-2.7.0RC1.tgz
    cd /tmp/xdebug-2.7.0RC1
    phpize
    ./configure
    make
    cp modules/xdebug.so /usr/lib/php/20180731
    cat <<EOF >> /etc/php/7.3/apache2/php.ini
zend_extension =/usr/lib/php/20180731/xdebug.so
[Xdebug]

xdebug.remote_enable = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_port = 9000
xdebug.idekey = "PHPSTORM"
xdebug.remote_host=33.33.33.1
xdebug.autostart=1
EOF
    service apache2 restart
fi