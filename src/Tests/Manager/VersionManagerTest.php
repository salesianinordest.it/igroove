<?php

namespace App\Tests\Controller;

require_once dirname(__DIR__) . '/../../../../app/AppKernel.php';

use PHPUnit\Framework\TestCase;
use App\Manager\VersionManager;

class VersionManagerTest extends TestCase
{

    /**
     * @var \Symfony\Component\HttpKernel\Kernel
     */
    protected $kernel;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var \App\Manager\VersionManager
     */
    protected $versionManager;

    protected $jmsSerializer;

    /**
     * @var \App\Manager\ConfigurationManager
     */
    protected $configuration;


    /**
     * @return null
     */
    public function setUp()
    {
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        $this->container = $this->kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();

        $this->jmsSerializer = $this->container->get('jms_serializer');
        $this->configuration = $this->container->get('zen.igroove.configuration');


    }


    /**
     * @test
     */
    public function checkNewVersion()
    {
        $versionManager = new VersionManager(
            $this->kernel->getRootDir(),
            $this->jmsSerializer,
            $this->configuration,
            $this->em
        );

        $versionManager->checkNewVersion();
        $this->assertFileExists($this->kernel->getRootDir() . '/../version.txt');
        $rows = file($this->kernel->getRootDir() . '/../version.txt');
        $this->assertCount(2, $rows);
        foreach ($rows as $line) {
            $this->assertEquals('v', substr($line, 0, 1));
        }
    }

}
