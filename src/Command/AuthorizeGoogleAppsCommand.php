<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Provider;
use App\Entity\Student;
use App\LdapTool;
use adLDAP\adLDAP;
use App\Manager\GoogleApps;

class AuthorizeGoogleAppsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('gmail:authorize')
            ->setDescription('Check if authorization for the google apps domain is neaded');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $providers = $em->getRepository('App:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {
            if ($provider->getStudentGoogleAppDomain()) {
                $output->writeln("<info>" . $provider->getStudentGoogleAppDomain() . "</info>");
                $googleApps = new GoogleApps(
                    $provider->getStudentGoogleAppDomain(),
                    $provider->getStudentGoogleAppClientId(),
                    $provider->getStudentGoogleAppclientSecret(),
                    $provider->getStudentGoogleAppOUPath(),
                    '/var/www/igroove/src',
                    $logger
                );
            }

            if ($provider->getTeacherGoogleAppDomain()) {
                $output->writeln("<info>" . $provider->getTeacherGoogleAppDomain() . "</info>");
                $googleApps = new GoogleApps(
                    $provider->getTeacherGoogleAppDomain(),
                    $provider->getTeacherGoogleAppClientId(),
                    $provider->getTeacherGoogleAppclientSecret(),
                    $provider->getTeacherGoogleAppOUPath(),
                    '/var/www/igroove/src',
                    $logger
                );
            }
        }
    }
}