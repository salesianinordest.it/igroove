<?php
namespace App\Command;

use App\Manager\VersionManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class VersionCommand extends Command
{

    public function __construct(VersionManager $versionManager)
    {
        $this->versionManager=$versionManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('version')
            ->setDescription('Check versions (your and avaibility');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $versions = $this->versionManager->checkNewVersion();
        $versions = $this->versionManager->getVersions();
        var_dump($versions);

    }
}
