<?php

namespace App\Command;

use App\Manager\CoaManager;
use App\Manager\LdapProxy;
use App\Manager\MikrotikManager;
use App\Manager\PersonsAndGroups;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\LdapGroup;
use App\LdapTool;
use App\MikrotikPool;
use adLDAP\adLDAP;
use App\Manager\InternetAccessManager;
use App\Manager\ConfigurationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class InternetCommand extends ContainerAwareCommand
{

    /**
     * @var \App\Manager\PersonsAndGroups
     */
    private $personsAndGroups;
    /**
     * @var \App\Manager\LdapProxy
     */
    private $ldapProxy;
    private $configurationManager;
    private $em;
    protected $logger;
    protected $container;
    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * @var \App\Repository\InternetOpenRepository
     */
    protected $internetOpenRepository;
    /**
     * @var \App\Repository\LdapGroupRepository
     */
    protected $ldapGroupRepository;
    protected $adGeneratedGroupPrefix;
    protected $ldapNeedSync = false;
    protected $mikrotikManager;
    protected $coaManager;

    public function __construct(
        ContainerInterface $container,
        LoggerInterface $logger,
        EntityManagerInterface $em,
        ConfigurationManager $configurationManager,
        PersonsAndGroups $personsAndGroups,
        LdapProxy $ldapProxy,
        MikrotikManager $mikrotikManager,
        CoaManager $coaManager
    )
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->personsAndGroups =$personsAndGroups;
        $this->ldapProxy = $ldapProxy;
        $this->container=$container;
        $this->mikrotikManager=$mikrotikManager;
        $this->coaManager = $coaManager;
        parent::__construct();
    }
    protected function configure()
    {
        $this
            ->setName('internet')
            ->setDescription('Check default internet behavior');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ldapProxy->connect();

        $this->output = $output;
        $this->logger = $this->getContainer()->get('logger');
        $this->printAndLogInfo("Starting internet command");
        $this->adGeneratedGroupPrefix = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $this->internetOpenRepository = $this->em->getRepository('App:InternetOpen');
        $this->ldapGroupRepository = $this->em->getRepository('App:LdapGroup');

        $this->printAndLogInfo("-Current active users, groups and ip lists:");
        $currentActiveEntities = $this->internetOpenRepository->findAll();
        $currentActiveEntitiesByType = ['group' => [], 'user' => [], 'ipList' => []];
        foreach($currentActiveEntities as $entity) {
            $currentActiveEntitiesByType[$entity->getType()][] = $entity->getAccount();
        }

        foreach ($currentActiveEntitiesByType as $type => $accounts) {
            if(empty($accounts)) {
                continue;
            }

            $this->printAndLogInfo("--[{$type}] ".implode(" ", $accounts));
        }

        $this->checkProvidersActionsAndStatus();
        $this->checkInternetOpensTimeout();

        if($this->ldapNeedSync) {
            $this->em->flush();
            $this->printAndLogInfo("-LdapGroup Updated");

            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
            $ldapclient = $this->getContainer()->get('old_sound_rabbit_mq.ldap_service_producer');
            $ldapclient->publish(serialize($msg));
        }

        $this->printAndLogInfo("-Mikrotik KickOffUsers");
        $this->mikrotikManager->KickOffUsers();
/*
        if($this->configurationManager->getWirelessUnifiController() != "" && $this->configurationManager->getWirelessUnifiKickExpired()) {
            $unifiManager = $this->getContainer()->get('zen.igroove.unifi');
            $this->printAndLogInfo("-Unifi kickOffClientExpiredClients");
            try {
                $unifiManager->kickOffClientExpiredClients();
            } catch (\Exception $e) {
                $this->printAndLogInfo("--Errore durante Unifi kickOffClientExpiredClients: ".$e->getMessage());
            }
        }
        */

        $this->printAndLogInfo("All done!");
        echo PHP_EOL;

        $this->em->getRepository('App:Cron')->setLatestRun('internet');
    }
    
    protected function checkProvidersActionsAndStatus()
    {
        //check available command to execute in providers
        $providers = $this->em->getRepository('App:Provider')->findBy(array('active' => true));
        $nowOneMinuteDown = new \DateTime('now -1 minutes');
        $nowOneMinuteUp = new \DateTime('now +1 minutes');
        $commandsToSet = array();

        foreach ($providers as $provider) {
            $rows = explode("\r\n", trim($provider->getInternetOpenAccessRange()));
            foreach ($rows as $row) {
                if (strlen(trim($row)) == 0) {
                    continue;
                }
                list($time, $check) = explode(": ", trim($row));
                $time = trim($time);
                $check = trim($check);
                $timeToCheck = new \DateTime($time);
                if (($timeToCheck >= $nowOneMinuteDown) and ($timeToCheck <= $nowOneMinuteUp)) {
                    $commandsToSet[$provider->getName()] = $check;
                }
            }
        }

        //get current internet access group member
        $internetAccessGroup = $this->getInternetAccessGroup();

        //cycle command to set from provider
        $this->printAndLogInfo("-Check providers command");
        foreach ($commandsToSet as $name => $toSet) {
            $providerGroupMembers = $this->getLdapGroupMembers($name);
            if(empty($providerGroupMembers->group) || !is_array($providerGroupMembers->group))
                continue;

            if ($toSet == 'on') {
                //add all classroom groups in the provider into the internet access group
                foreach ($providerGroupMembers->group as $groupName) {
                    $internetAccessGroup->addMember('group', $groupName);
                }

                $this->ldapNeedSync = true;
                $this->printAndLogInfo("--Groups added to internetaccess group: ".implode(",",$providerGroupMembers->group));
            } elseif($toSet == 'off') {
                foreach($providerGroupMembers->group as $providerClassroomGroup) {
                    $providerClassroomGroupMembers = $this->getLdapGroupMembers($providerClassroomGroup);

                    //remove user in current classroom group from internetaccess group and internetopen table
                    if(is_array($providerClassroomGroupMembers->user)) {
                        foreach($providerClassroomGroupMembers->user as $providerClassroomGroupUser) {
                            $iouEntities = $this->internetOpenRepository->findBy(array('type' => 'user', 'account' => strtolower($providerClassroomGroupUser)));
                            foreach ($iouEntities as $entity) {
                                $this->em->remove($entity);
                            }

                            $internetAccessGroup->removeMember('user', $providerClassroomGroupUser);
                            $this->coaManager->KickOffUser($providerClassroomGroupUser);


                        }
                    }

                    //remove the classroom group from internetaccess group and internetopen table
                    $internetAccessGroup->removeMember('group', $providerClassroomGroup);
                    $this->removeCoaFromGroup($providerClassroomGroup);

                    $iogEntities = $this->internetOpenRepository->findBy(array('type' => 'group', 'account' => strtolower($providerClassroomGroup)));
                    foreach ($iogEntities as $entity) {
                        $this->em->remove($entity);
                    }
                    $this->em->flush();

                    $this->ldapNeedSync = true;
                    $this->printAndLogInfo("--Group {$providerClassroomGroup} and his users removed from internetaccess group");
                }
            }
        }
    }

    protected function checkInternetOpensTimeout()
    {
        $internetAccessGroup = $this->getInternetAccessGroup();
        $personalDeviceAccessGroup = $this->getPersonalDeviceAccessGroup();

        //query the expired activation
        $this->printAndLogInfo("-Check single user, group and ip list timeout from internetaccess table");
        $date = new \DateTime();
        $query = $this->internetOpenRepository
            ->createQueryBuilder('i')
            ->select('i.id, i.type, i.account, i.permitPersonalDevices')
            ->where(' i.close_at <= :date')
            ->setParameter('date', $date)
            ->getQuery();
        $internetOpens = $query->getResult();

        $updateIpList = false;
        foreach ($internetOpens as $internetOpen) {
            if ($internetOpen['type'] == 'group') {
                $this->printAndLogInfo("--remove group: " . $internetOpen['account']);

                if($internetAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('group', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }

                if($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('group', $internetOpen['account']);

                    $this->removeCoaFromGroup($internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif ($internetOpen['type'] == 'user') {
                $this->printAndLogInfo("--remove user " . $internetOpen['account']);

                if($internetAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('user', $internetOpen['account']);


                    $this->coaManager->KickOffUser($internetOpen['account']);

                    $this->ldapNeedSync = true;
                }

                if($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('user', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif($internetOpen['type'] == 'ipList') {
                $this->printAndLogInfo("--remove iplist " . $internetOpen['account']);
                $updateIpList = true;
            }

            //remove the internetOpen entity
            $internetOpen = $this->em->getReference('App:InternetOpen', $internetOpen['id']);
            $this->em->remove($internetOpen);
            $this->em->flush();
        }

        if($updateIpList) {
            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = array('command' => 'addIpToBypassedList', 'parameters' => array());
            $mktclient = $this->getContainer()->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $mktclient->publish(serialize($msg));
        }
    }

    protected function getInternetAccessGroup() {
        $internetAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'InternetAccess']);
        if(!$internetAccessGroup instanceof LdapGroup) {
            $internetAccessGroup = $this->personsAndGroups->checkInternetAccessLdapGroups();
        }

        return $internetAccessGroup;
    }

    protected function getPersonalDeviceAccessGroup() {
        $personalDeviceAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'PersonalDeviceAccess']);
        if(!$personalDeviceAccessGroup instanceof LdapGroup) {
            $personalDeviceAccessGroup = $this->personsAndGroups->checkPersonalDeviceAccessLdapGroups();
        }

        return $personalDeviceAccessGroup;
    }

    protected function getLdapGroupMembers($nameOrGroup) {
        $groupMembers = $group = null;
        if(is_string($nameOrGroup)) {
            $group = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . $this->ldapGroupRepository::ldapEscape($nameOrGroup)]);
        } elseif($nameOrGroup instanceof LdapGroup) {
            $group = $nameOrGroup;
        }

        if($group instanceof LdapGroup && $group->getMembers() != "") {
            $groupMembers = json_decode($group->getMembers());
        }

        if($groupMembers === null) {
            $groupMembers = new \stdClass();
        }

        if(!isset($groupMembers->group) || $groupMembers->group == null) {
            $groupMembers->group = [];
        } elseif (is_object($groupMembers->group)) {
            $groupMembers->group = (array)$groupMembers->group;
        }

        if(!isset($groupMembers->user)) {
            $groupMembers->user = [];
        } elseif (is_object($groupMembers->user)) {
            $groupMembers->user = (array)$groupMembers->user;
        }

        return $groupMembers;
    }

    protected function printAndLogInfo($message) {
        $this->output->writeln('<info>'.$message.'</info>');
        $this->logger->info("CRON:".$message);
    }

    private function removeCoaFromGroup($groupName)
    {
        $usernames = $this->em->getRepository('App:LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
            $this->coaManager->KickOffUser($username);
        }
    }
}
