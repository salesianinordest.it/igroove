<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 */
class Provider
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_filter", "provider_full"})
     */
    private $filter;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true)
     * Serializer\Groups({"provider_filter", "provider_full"})
     */
    private $filterData;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;



    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentAutoCreateUsername;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentForceImportedPassword;


    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentForceImportedPasswordInitial;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, options={"default": "lastname"})
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentsOrderByInTeacherList;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentLdapCreateOU;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentLdapOUPrefix;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentLdapOUPath;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $studentLdapExtendedUsernameSuffix;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $studentLdapUseEmailForExtendedUsername;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentLdapAutoUsernameStyle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppDomain;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppClientId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppClientSecret;

    /**
     * @var bool
     * @ORM\Column(type="integer", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppAutoEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppAutoEmailStyle;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppCreateGroup;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppCreateProviderGroup;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppGroupPrefix;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppGroupExtraEmail;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppProviderGroupExtraEmail;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppUseUserInProviderGroup;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppCreateOU;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppOUPrefix;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $studentGoogleAppOUPath;



    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherAutoCreateUsername;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherForceImportedPassword;
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherForceImportedPasswordInitial;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherLdapCreateOU;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherLdapOUPrefix;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $teacherLdapAutoUsernameStyle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherLdapOUPath;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherLdapExtendedUsernameSuffix;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherLdapUseEmailForExtendedUsername;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppDomain;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppClientId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppClientSecret;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppAutoEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppAutoEmailStyle;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppCreateGroup;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppCreateProviderGroup;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppGroupPrefix;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppGroupExtraEmail;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppProviderGroupExtraEmail;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppUseUserInProviderGroup;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppCreateOU;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppOUPrefix;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_teacher_settings", "provider_full"})
     */
    private $teacherGoogleAppOUPath;



    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $internetTeachersControl;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $internetOpenAccessRange;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $pdfHeader;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $pdfFooter;

    /**
     * var integer
     * @ORM\Column(type="smallint", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $pdfHeaderHeight;

    /**
     * var integer
     * @ORM\Column(type="smallint", nullable=true)
     * Serializer\Groups({"provider_settings", "provider_full"})
     */
    private $pdfFooterHeight;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $pdfStudentBadge;

    /**
     * var string
     * @ORM\Column(type="string", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $pdfStudentBadgePageSize;

    /**
     * var boolean
     * @ORM\Column(type="boolean", nullable=true)
     * Serializer\Groups({"provider_student_settings", "provider_full"})
     */
    private $pdfStudentBadgePageLandscape;



    /**
     * @var ArrayCollection|Student[]
     * @ORM\OneToMany(targetEntity="Student", mappedBy="provider")
     * Serializer\Groups({"provider_students"})
     * Serializer\MaxDepth(2)
     **/
    private $students;

    /**
     * @var ArrayCollection|Group[]
     * @ORM\OneToMany(targetEntity="Group", mappedBy="provider")
     * Serializer\Groups({"provider_groups"})
     * Serializer\MaxDepth(3)
     **/
    private $groups;

    /**
     * @var ArrayCollection|Sector[]
     * @ORM\OneToMany(targetEntity="Sector", mappedBy="provider")
     * Serializer\Groups({"provider_sectors"})
     * Serializer\MaxDepth(2)
     **/
    private $sectors;

    /**
     * @var ArrayCollection|Subject[]
     * @ORM\OneToMany(targetEntity="Subject", mappedBy="provider")
     * Serializer\Groups({"provider_subjects"})
     * Serializer\MaxDepth(2)
     **/
    private $subjects;

    /**
     * @var ArrayCollection|Teacher[]
     * @ORM\OneToMany(targetEntity="Teacher", mappedBy="provider")
     * Serializer\Groups({"provider_teachers"})
     * Serializer\MaxDepth(2)
     **/
    private $teachers;

    /**
     * @var ArrayCollection|Student[]
     * @ORM\ManyToMany(targetEntity="Student", mappedBy="additionalProviders")
     * Serializer\Groups({"provider_students"})
     * Serializer\MaxDepth(2)
     */
    protected $additionalStudents;

    /**
     * @var ArrayCollection|Teacher[]
     * @ORM\ManyToMany(targetEntity="Teacher", mappedBy="additionalProviders")
     * Serializer\Groups({"provider_teachers"})
     * Serializer\MaxDepth(2)
     */
    protected $additionalTeachers;

    /**
     * @var ArrayCollection|TeacherSubjectGroup[]
     * @ORM\OneToMany(targetEntity="TeacherSubjectGroup", mappedBy="provider")
     * Serializer\Groups({"provider_teacher_subject_group"})
     * Serializer\MaxDepth(2)
     **/
    private $teacherSubjectGroups;

    /**
     * @var AppleSchoolLocation
     * @ORM\ManyToOne(targetEntity="AppleSchoolLocation", inversedBy="providers")
     * Serializer\Groups({"provider_apple_school"})
     * Serializer\MaxDepth(2)
     **/
    private $appleSchoolLocation;


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->students = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subjects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sectors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->additionalStudents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->additionalTeachers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teacherSubjectGroups = new ArrayCollection();
    }

    public function __clone() {
        $this->students = clone $this->students;
        $this->teachers = clone $this->teachers;
        $this->groups = clone $this->groups;
        $this->subjects = clone $this->subjects;
        $this->sectors = clone $this->sectors;
        $this->additionalStudents = clone $this->additionalStudents;
        $this->additionalTeachers = clone $this->additionalTeachers;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFilter() {
        return $this->filter;
    }

    /**
     * @param string $filter
     */
    public function setFilter($filter) {
        $this->filter = $filter;
    }

    /**
     * @return array
     */
    public function getFilterData() {
        return $this->filterData;
    }

    /**
     * @param array $filterData
     */
    public function setFilterData($filterData) {
        $this->filterData = $filterData;
    }

    /**
     * @return bool
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function getStudentAutoCreateUsername() {
        return $this->studentAutoCreateUsername;
    }

    /**
     * @param bool $studentAutoCreateUsername
     */
    public function setStudentAutoCreateUsername($studentAutoCreateUsername) {
        $this->studentAutoCreateUsername = $studentAutoCreateUsername;
    }

    /**
     * @return boolean
     */
    public function getStudentForceImportedPassword() {
        return $this->studentForceImportedPassword;
    }

    /**
     * @param boolean $studentForceImportedPassword
     */
    public function setStudentForceImportedPassword($studentForceImportedPassword) {
        $this->studentForceImportedPassword = $studentForceImportedPassword;
    }

    /**
     * @return string
     */
    public function getStudentsOrderByInTeacherList() {
        return $this->studentsOrderByInTeacherList;
    }

    /**
     * @param string $studentsOrderByInTeacherList
     */
    public function setStudentsOrderByInTeacherList($studentsOrderByInTeacherList) {
        $this->studentsOrderByInTeacherList = $studentsOrderByInTeacherList;
    }

    /**
     * @return bool
     */
    public function isStudentLdapCreateOU() {
        return $this->studentLdapCreateOU;
    }

    /**
     * @param bool $studentLdapCreateOU
     */
    public function setStudentLdapCreateOU($studentLdapCreateOU) {
        $this->studentLdapCreateOU = $studentLdapCreateOU;
    }

    /**
     * @return string
     */
    public function getStudentLdapOUPrefix() {
        return $this->studentLdapOUPrefix;
    }

    /**
     * @param string $studentLdapOUPrefix
     */
    public function setStudentLdapOUPrefix($studentLdapOUPrefix) {
        $this->studentLdapOUPrefix = $studentLdapOUPrefix;
    }

    /**
     * @return string
     */
    public function getStudentLdapOUPath() {
        return $this->studentLdapOUPath;
    }

    /**
     * @param string $studentLdapOUPath
     */
    public function setStudentLdapOUPath($studentLdapOUPath) {
        $this->studentLdapOUPath = $studentLdapOUPath;
    }

    /**
     * @return string|null
     */
    public function getStudentLdapExtendedUsernameSuffix(): ?string {
        return $this->studentLdapExtendedUsernameSuffix;
    }

    /**
     * @param string|null $studentLdapExtendedUsernameSuffix
     */
    public function setStudentLdapExtendedUsernameSuffix(?string $studentLdapExtendedUsernameSuffix): void {
        $this->studentLdapExtendedUsernameSuffix = $studentLdapExtendedUsernameSuffix;
    }

    /**
     * @return bool
     */
    public function isStudentLdapUseEmailForExtendedUsername(): bool {
        return (bool)$this->studentLdapUseEmailForExtendedUsername;
    }

    /**
     * @param bool $studentLdapUseEmailForExtendedUsername
     */
    public function setStudentLdapUseEmailForExtendedUsername(bool $studentLdapUseEmailForExtendedUsername): void {
        $this->studentLdapUseEmailForExtendedUsername = $studentLdapUseEmailForExtendedUsername;
    }

    /**
     * @return string
     */
    public function getStudentLdapAutoUsernameStyle() {
        return $this->studentLdapAutoUsernameStyle;
    }

    /**
     * @param string $studentLdapAutoUsernameStyle
     */
    public function setStudentLdapAutoUsernameStyle($studentLdapAutoUsernameStyle) {
        $this->studentLdapAutoUsernameStyle = $studentLdapAutoUsernameStyle;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppDomain() {
        return $this->studentGoogleAppDomain;
    }

    /**
     * @param string $studentGoogleAppDomain
     */
    public function setStudentGoogleAppDomain($studentGoogleAppDomain) {
        $this->studentGoogleAppDomain = $studentGoogleAppDomain;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppClientId() {
        return $this->studentGoogleAppClientId;
    }

    /**
     * @param string $studentGoogleAppClientId
     */
    public function setStudentGoogleAppClientId($studentGoogleAppClientId) {
        $this->studentGoogleAppClientId = $studentGoogleAppClientId;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppClientSecret() {
        return $this->studentGoogleAppClientSecret;
    }

    /**
     * @param string $studentGoogleAppClientSecret
     */
    public function setStudentGoogleAppClientSecret($studentGoogleAppClientSecret) {
        $this->studentGoogleAppClientSecret = $studentGoogleAppClientSecret;
    }

    /**
     * @return integer
     */
    public function getStudentGoogleAppAutoEmail() {
        return $this->studentGoogleAppAutoEmail;
    }

    /**
     * @param integer $studentGoogleAppAutoEmail
     */
    public function setStudentGoogleAppAutoEmail($studentGoogleAppAutoEmail) {
        $this->studentGoogleAppAutoEmail = $studentGoogleAppAutoEmail;
    }

    /**
     * @return string|null
     */
    public function getStudentGoogleAppAutoEmailStyle() {
        return $this->studentGoogleAppAutoEmailStyle;
    }

    /**
     * @param string|null $studentGoogleAppAutoEmailStyle
     */
    public function setStudentGoogleAppAutoEmailStyle($studentGoogleAppAutoEmailStyle) {
        $this->studentGoogleAppAutoEmailStyle = $studentGoogleAppAutoEmailStyle;
    }

    /**
     * @return boolean
     */
    public function getStudentGoogleAppCreateGroup() {
        return $this->studentGoogleAppCreateGroup;
    }

    /**
     * @param boolean $studentGoogleAppCreateGroup
     */
    public function setStudentGoogleAppCreateGroup($studentGoogleAppCreateGroup) {
        $this->studentGoogleAppCreateGroup = $studentGoogleAppCreateGroup;
    }

    /**
     * @return bool
     */
    public function isStudentGoogleAppCreateProviderGroup() {
        return $this->studentGoogleAppCreateProviderGroup;
    }

    /**
     * @param bool $studentGoogleAppCreateProviderGroup
     */
    public function setStudentGoogleAppCreateProviderGroup($studentGoogleAppCreateProviderGroup) {
        $this->studentGoogleAppCreateProviderGroup = $studentGoogleAppCreateProviderGroup;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppGroupPrefix() {
        return $this->studentGoogleAppGroupPrefix;
    }

    /**
     * @param string $studentGoogleAppGroupPrefix
     */
    public function setStudentGoogleAppGroupPrefix($studentGoogleAppGroupPrefix) {
        $this->studentGoogleAppGroupPrefix = $studentGoogleAppGroupPrefix;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppGroupExtraEmail() {
        return $this->studentGoogleAppGroupExtraEmail;
    }

    /**
     * @param string $studentGoogleAppGroupExtraEmail
     */
    public function setStudentGoogleAppGroupExtraEmail($studentGoogleAppGroupExtraEmail) {
        $this->studentGoogleAppGroupExtraEmail = $studentGoogleAppGroupExtraEmail;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppProviderGroupExtraEmail() {
        return $this->studentGoogleAppProviderGroupExtraEmail;
    }

    /**
     * @param string $studentGoogleAppProviderGroupExtraEmail
     */
    public function setStudentGoogleAppProviderGroupExtraEmail($studentGoogleAppProviderGroupExtraEmail) {
        $this->studentGoogleAppProviderGroupExtraEmail = $studentGoogleAppProviderGroupExtraEmail;
    }

    /**
     * @return bool
     */
    public function isStudentGoogleAppUseUserInProviderGroup() {
        return $this->studentGoogleAppUseUserInProviderGroup;
    }

    /**
     * @param bool $studentGoogleAppUseUserInProviderGroup
     */
    public function setStudentGoogleAppUseUserInProviderGroup($studentGoogleAppUseUserInProviderGroup) {
        $this->studentGoogleAppUseUserInProviderGroup = $studentGoogleAppUseUserInProviderGroup;
    }

    /**
     * @return boolean
     */
    public function getStudentGoogleAppCreateOU() {
        return $this->studentGoogleAppCreateOU;
    }

    /**
     * @param boolean $studentGoogleAppCreateOU
     */
    public function setStudentGoogleAppCreateOU($studentGoogleAppCreateOU) {
        $this->studentGoogleAppCreateOU = $studentGoogleAppCreateOU;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppOUPrefix() {
        return $this->studentGoogleAppOUPrefix;
    }

    /**
     * @param string $studentGoogleAppOUPrefix
     */
    public function setStudentGoogleAppOUPrefix($studentGoogleAppOUPrefix) {
        $this->studentGoogleAppOUPrefix = $studentGoogleAppOUPrefix;
    }

    /**
     * @return string
     */
    public function getStudentGoogleAppOUPath() {
        return $this->studentGoogleAppOUPath!=""? $this->studentGoogleAppOUPath : "/";
    }

    /**
     * @param string $studentGoogleAppOUPath
     */
    public function setStudentGoogleAppOUPath($studentGoogleAppOUPath) {
        $this->studentGoogleAppOUPath = $studentGoogleAppOUPath;
    }


    /**
     * All the settings related to a teacher
     * @return array
     */
    public function getStudentSettings() {
        return [
            'autoCreateUsername' => $this->studentAutoCreateUsername,
            'forceImportedPassword' => $this->studentForceImportedPassword,
            'forceImportedPasswordInitial' => $this->getStudentForceImportedPasswordInitial(),
            'ldapCreateOU' => $this->studentLdapCreateOU,
            'ldapOUPrefix' => $this->studentLdapOUPrefix,
            'ldapOUPath' => $this->studentLdapOUPath,
            'ldapExtendedUsernameSuffix' => $this->studentLdapExtendedUsernameSuffix,
            'ldapUseEmailForExtendedUsername' => $this->studentLdapUseEmailForExtendedUsername,
            'ldapAutoUsernameStyle' => $this->studentLdapAutoUsernameStyle,
            'googleAppDomain' => $this->studentGoogleAppDomain,
            'googleAppClientId' => $this->studentGoogleAppClientId,
            'googleAppClientSecret' => $this->studentGoogleAppClientSecret,
            'googleAppAutoEmail' => $this->studentGoogleAppAutoEmail,
            'googleAppAutoEmailStyle' => $this->studentGoogleAppAutoEmailStyle,
            'googleAppCreateGroup' => $this->studentGoogleAppCreateGroup,
            'googleAppCreateProviderGroup' => $this->studentGoogleAppCreateProviderGroup,
            'googleAppGroupPrefix' => $this->studentGoogleAppGroupPrefix,
            'googleAppGroupExtraEmail' => $this->studentGoogleAppGroupExtraEmail,
            'googleAppProviderGroupExtraEmail' => $this->studentGoogleAppProviderGroupExtraEmail,
            'googleAppUseUserInProviderGroup' => $this->studentGoogleAppUseUserInProviderGroup,
            'googleAppCreateOU' => $this->studentGoogleAppCreateOU,
            'googleAppOUPrefix' => $this->studentGoogleAppOUPrefix,
            'googleAppOUPath' => $this->studentGoogleAppOUPath,
        ];
    }


    /**
     * @return boolean
     */
    public function getTeacherAutoCreateUsername() {
        return $this->teacherAutoCreateUsername;
    }

    /**
     * @param boolean $teacherAutoCreateUsername
     */
    public function setTeacherAutoCreateUsername($teacherAutoCreateUsername) {
        $this->teacherAutoCreateUsername = $teacherAutoCreateUsername;
    }

    /**
     * @return boolean
     */
    public function getTeacherForceImportedPassword() {
        return $this->teacherForceImportedPassword;
    }

    /**
     * @param boolean $teacherForceImportedPassword
     */
    public function setTeacherForceImportedPassword($teacherForceImportedPassword) {
        $this->teacherForceImportedPassword = $teacherForceImportedPassword;
    }

    /**
     * @return bool
     */
    public function isTeacherLdapCreateOU() {
        return $this->teacherLdapCreateOU;
    }

    /**
     * @param bool $teacherLdapCreateOU
     */
    public function setTeacherLdapCreateOU($teacherLdapCreateOU) {
        $this->teacherLdapCreateOU = $teacherLdapCreateOU;
    }

    /**
     * @return string
     */
    public function getTeacherLdapOUPrefix() {
        return $this->teacherLdapOUPrefix;
    }

    /**
     * @param string $teacherLdapOUPrefix
     */
    public function setTeacherLdapOUPrefix($teacherLdapOUPrefix) {
        $this->teacherLdapOUPrefix = $teacherLdapOUPrefix;
    }

    /**
     * @return string
     */
    public function getTeacherLdapOUPath() {
        return $this->teacherLdapOUPath;
    }

    /**
     * @param string $teacherLdapOUPath
     */
    public function setTeacherLdapOUPath($teacherLdapOUPath) {
        $this->teacherLdapOUPath = $teacherLdapOUPath;
    }

    /**
     * @return string|null
     */
    public function getTeacherLdapExtendedUsernameSuffix(): ?string {
        return $this->teacherLdapExtendedUsernameSuffix;
    }

    /**
     * @param string|null $teacherLdapExtendedUsernameSuffix
     */
    public function setTeacherLdapExtendedUsernameSuffix(?string $teacherLdapExtendedUsernameSuffix): void {
        $this->teacherLdapExtendedUsernameSuffix = $teacherLdapExtendedUsernameSuffix;
    }

    /**
     * @return bool
     */
    public function isTeacherLdapUseEmailForExtendedUsername(): bool {
        return (bool)$this->teacherLdapUseEmailForExtendedUsername;
    }

    /**
     * @param bool $teacherLdapUseEmailForExtendedUsername
     */
    public function setTeacherLdapUseEmailForExtendedUsername(bool $teacherLdapUseEmailForExtendedUsername): void {
        $this->teacherLdapUseEmailForExtendedUsername = $teacherLdapUseEmailForExtendedUsername;
    }

    /**
     * @return string
     */
    public function getTeacherLdapAutoUsernameStyle() {
        return $this->teacherLdapAutoUsernameStyle;
    }

    /**
     * @param string $teacherLdapAutoUsernameStyle
     */
    public function setTeacherLdapAutoUsernameStyle($teacherLdapAutoUsernameStyle) {
        $this->teacherLdapAutoUsernameStyle = $teacherLdapAutoUsernameStyle;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppDomain() {
        return $this->teacherGoogleAppDomain;
    }

    /**
     * @param string $teacherGoogleAppDomain
     */
    public function setTeacherGoogleAppDomain($teacherGoogleAppDomain) {
        $this->teacherGoogleAppDomain = $teacherGoogleAppDomain;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppClientId() {
        return $this->teacherGoogleAppClientId;
    }

    /**
     * @param string $teacherGoogleAppClientId
     */
    public function setTeacherGoogleAppClientId($teacherGoogleAppClientId) {
        $this->teacherGoogleAppClientId = $teacherGoogleAppClientId;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppClientSecret() {
        return $this->teacherGoogleAppClientSecret;
    }

    /**
     * @param string $teacherGoogleAppClientSecret
     */
    public function setTeacherGoogleAppClientSecret($teacherGoogleAppClientSecret) {
        $this->teacherGoogleAppClientSecret = $teacherGoogleAppClientSecret;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppAutoEmail() {
        return $this->teacherGoogleAppAutoEmail;
    }

    /**
     * @param string $teacherGoogleAppAutoEmail
     */
    public function setTeacherGoogleAppAutoEmail($teacherGoogleAppAutoEmail) {
        $this->teacherGoogleAppAutoEmail = $teacherGoogleAppAutoEmail;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppAutoEmailStyle() {
        return $this->teacherGoogleAppAutoEmailStyle;
    }

    /**
     * @param string $teacherGoogleAppAutoEmailStyle
     */
    public function setTeacherGoogleAppAutoEmailStyle($teacherGoogleAppAutoEmailStyle) {
        $this->teacherGoogleAppAutoEmailStyle = $teacherGoogleAppAutoEmailStyle;
    }

    /**
     * @return boolean
     */
    public function getTeacherGoogleAppCreateGroup() {
        return $this->teacherGoogleAppCreateGroup;
    }

    /**
     * @param boolean $teacherGoogleAppCreateGroup
     */
    public function setTeacherGoogleAppCreateGroup($teacherGoogleAppCreateGroup) {
        $this->teacherGoogleAppCreateGroup = $teacherGoogleAppCreateGroup;
    }

    /**
     * @return bool
     */
    public function isTeacherGoogleAppCreateProviderGroup() {
        return $this->teacherGoogleAppCreateProviderGroup;
    }

    /**
     * @param bool $teacherGoogleAppCreateProviderGroup
     */
    public function setTeacherGoogleAppCreateProviderGroup($teacherGoogleAppCreateProviderGroup) {
        $this->teacherGoogleAppCreateProviderGroup = $teacherGoogleAppCreateProviderGroup;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppGroupPrefix() {
        return $this->teacherGoogleAppGroupPrefix;
    }

    /**
     * @param string $teacherGoogleAppGroupPrefix
     */
    public function setTeacherGoogleAppGroupPrefix($teacherGoogleAppGroupPrefix) {
        $this->teacherGoogleAppGroupPrefix = $teacherGoogleAppGroupPrefix;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppGroupExtraEmail() {
        return $this->teacherGoogleAppGroupExtraEmail;
    }

    /**
     * @param string $teacherGoogleAppGroupExtraEmail
     */
    public function setTeacherGoogleAppGroupExtraEmail($teacherGoogleAppGroupExtraEmail) {
        $this->teacherGoogleAppGroupExtraEmail = $teacherGoogleAppGroupExtraEmail;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppProviderGroupExtraEmail() {
        return $this->teacherGoogleAppProviderGroupExtraEmail;
    }

    /**
     * @param string $teacherGoogleAppProviderGroupExtraEmail
     */
    public function setTeacherGoogleAppProviderGroupExtraEmail($teacherGoogleAppProviderGroupExtraEmail) {
        $this->teacherGoogleAppProviderGroupExtraEmail = $teacherGoogleAppProviderGroupExtraEmail;
    }

    /**
     * @return bool
     */
    public function isTeacherGoogleAppUseUserInProviderGroup() {
        return $this->teacherGoogleAppUseUserInProviderGroup;
    }

    /**
     * @param bool $teacherGoogleAppUseUserInProviderGroup
     */
    public function setTeacherGoogleAppUseUserInProviderGroup($teacherGoogleAppUseUserInProviderGroup) {
        $this->teacherGoogleAppUseUserInProviderGroup = $teacherGoogleAppUseUserInProviderGroup;
    }

    /**
     * @return boolean
     */
    public function getTeacherGoogleAppCreateOU() {
        return $this->teacherGoogleAppCreateOU;
    }

    /**
     * @param boolean $teacherGoogleAppCreateOU
     */
    public function setTeacherGoogleAppCreateOU($teacherGoogleAppCreateOU) {
        $this->teacherGoogleAppCreateOU = $teacherGoogleAppCreateOU;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppOUPrefix() {
        return $this->teacherGoogleAppOUPrefix;
    }

    /**
     * @param string $teacherGoogleAppOUPrefix
     */
    public function setTeacherGoogleAppOUPrefix($teacherGoogleAppOUPrefix) {
        $this->teacherGoogleAppOUPrefix = $teacherGoogleAppOUPrefix;
    }

    /**
     * @return string
     */
    public function getTeacherGoogleAppOUPath() {
        return $this->teacherGoogleAppOUPath!=""? $this->teacherGoogleAppOUPath : "/";
    }

    /**
     * @param string $teacherGoogleAppOUPath
     */
    public function setTeacherGoogleAppOUPath($teacherGoogleAppOUPath) {
        $this->teacherGoogleAppOUPath = $teacherGoogleAppOUPath;
    }

    /**
     * @return boolean
     */
    public function getInternetTeachersControl() {
        return $this->internetTeachersControl;
    }

    /**
     * @param boolean $internetTeachersControl
     */
    public function setInternetTeachersControl($internetTeachersControl) {
        $this->internetTeachersControl = $internetTeachersControl;
    }


    /**
     * All the settings related to a teacher
     * @return array
     */
    public function getTeacherSettings() {
        return [
            'autoCreateUsername' => $this->teacherAutoCreateUsername,
            'forceImportedPassword' => $this->teacherForceImportedPassword,
            'forceImportedPasswordInitial' => $this->getTeacherForceImportedPasswordInitial(),
            'ldapCreateOU' => $this->teacherLdapCreateOU,
            'ldapOUPrefix' => $this->teacherLdapOUPrefix,
            'ldapOUPath' => $this->teacherLdapOUPath,
            'ldapExtendedUsernameSuffix' => $this->teacherLdapExtendedUsernameSuffix,
            'ldapUseEmailForExtendedUsername' => $this->teacherLdapUseEmailForExtendedUsername,
            'ldapAutoUsernameStyle' => $this->teacherLdapAutoUsernameStyle,
            'googleAppDomain' => $this->teacherGoogleAppDomain,
            'googleAppClientId' => $this->teacherGoogleAppClientId,
            'googleAppClientSecret' => $this->teacherGoogleAppClientSecret,
            'googleAppAutoEmail' => $this->teacherGoogleAppAutoEmail,
            'googleAppAutoEmailStyle' => $this->teacherGoogleAppAutoEmailStyle,
            'googleAppCreateGroup' => $this->teacherGoogleAppCreateGroup,
            'googleAppCreateProviderGroup' => $this->teacherGoogleAppCreateProviderGroup,
            'googleAppGroupPrefix' => $this->teacherGoogleAppGroupPrefix,
            'googleAppGroupExtraEmail' => $this->teacherGoogleAppGroupExtraEmail,
            'googleAppProviderGroupExtraEmail' => $this->teacherGoogleAppProviderGroupExtraEmail,
            'googleAppUseUserInProviderGroup' => $this->teacherGoogleAppUseUserInProviderGroup,
            'googleAppCreateOU' => $this->teacherGoogleAppCreateOU,
            'googleAppOUPrefix' => $this->teacherGoogleAppOUPrefix,
            'googleAppOUPath' => $this->teacherGoogleAppOUPath,
        ];
    }


    /**
     * @return string
     */
    public function getInternetOpenAccessRange() {
        return $this->internetOpenAccessRange;
    }

    /**
     * @param string $internetOpenAccessRange
     */
    public function setInternetOpenAccessRange($internetOpenAccessRange) {
        $this->internetOpenAccessRange = $internetOpenAccessRange;
    }




    /**
     * @return string
     */
    public function getPdfHeader() {
        return $this->pdfHeader;
    }

    /**
     * @param string $pdfHeader
     */
    public function setPdfHeader($pdfHeader) {
        $this->pdfHeader = $pdfHeader;
    }

    /**
     * @return string
     */
    public function getPdfFooter() {
        return $this->pdfFooter;
    }

    /**
     * @param string $pdfFooter
     */
    public function setPdfFooter($pdfFooter) {
        $this->pdfFooter = $pdfFooter;
    }

    /**
     * @return integer
     */
    public function getPdfHeaderHeight() {
        return $this->pdfHeaderHeight;
    }

    /**
     * @param integer $pdfHeaderHeight
     */
    public function setPdfHeaderHeight($pdfHeaderHeight) {
        $this->pdfHeaderHeight = $pdfHeaderHeight;
    }

    /**
     * @return integer
     */
    public function getPdfFooterHeight() {
        return $this->pdfFooterHeight;
    }

    /**
     * @param integer $pdfFooterHeight
     */
    public function setPdfFooterHeight($pdfFooterHeight) {
        $this->pdfFooterHeight = $pdfFooterHeight;
    }

    /**
     * @return string
     */
    public function getPdfStudentBadge() {
        return $this->pdfStudentBadge;
    }

    /**
     * @param string $pdfStudentBadge
     */
    public function setPdfStudentBadge($pdfStudentBadge) {
        $this->pdfStudentBadge = $pdfStudentBadge;
    }

    /**
     * @return string
     */
    public function getPdfStudentBadgePageSize() {
        return $this->pdfStudentBadgePageSize;
    }

    /**
     * @param string $pdfStudentBadgePageSize
     */
    public function setPdfStudentBadgePageSize($pdfStudentBadgePageSize) {
        $this->pdfStudentBadgePageSize = $pdfStudentBadgePageSize;
    }

    /**
     * @return boolean
     */
    public function getPdfStudentBadgePageLandscape() {
        return $this->pdfStudentBadgePageLandscape;
    }

    /**
     * @param boolean $pdfStudentBadgePageLandscape
     */
    public function setPdfStudentBadgePageLandscape($pdfStudentBadgePageLandscape) {
        $this->pdfStudentBadgePageLandscape = $pdfStudentBadgePageLandscape;
    }



    /**
     * Add student
     *
     * @param \App\Entity\Student $student
     *
     * @return Provider
     */
    public function addStudent(\App\Entity\Student $student)
    {
        $this->students[] = $student;

        return $this;
    }

    /**
     * Remove student
     *
     * @param \App\Entity\Student $student
     */
    public function removeStudent(\App\Entity\Student $student)
    {
        $this->students->removeElement($student);
    }

    /**
     * Get students
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Add group
     *
     * @param \App\Entity\Group $group
     *
     * @return Provider
     */
    public function addGroup(\App\Entity\Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \App\Entity\Group $group
     */
    public function removeGroup(\App\Entity\Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSectors() {
        return $this->sectors;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubjects() {
        return $this->subjects;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachers() {
        return $this->teachers;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|Student[]
     */
    public function getAdditionalStudents() {
        return $this->additionalStudents;
    }

    /**
     * @param Student $student
     */
    public function addAdditionalStudent(Student $student) {
        $this->additionalStudents->add($student);
        $student->addAdditionalProvider($this);
    }

    /**
     * @param Student $student
     */
    public function removeAdditionalStudent(Student $student) {
        $this->additionalStudents->removeElement($student);
        $student->removeAdditionalProvider($this);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|Teacher[]
     */
    public function getAdditionalTeachers() {
        return $this->additionalTeachers;
    }

    /**
     * @param Teacher $teacher
     */
    public function addAdditionalTeacher(Teacher $teacher) {
        $this->additionalStudents->add($teacher);
        $teacher->addAdditionalProvider($this);
    }

    /**
     * @param Teacher $teacher
     */
    public function removeAdditionalTeacher(Teacher $teacher) {
        $this->additionalStudents->removeElement($teacher);
        $teacher->removeAdditionalProvider($this);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeacherSubjectGroups() {
        return $this->teacherSubjectGroups;
    }

    /**
     * @return AppleSchoolLocation
     */
    public function getAppleSchoolLocation() {
        return $this->appleSchoolLocation;
    }

    /**
     * @param AppleSchoolLocation $appleSchoolLocation
     */
    public function setAppleSchoolLocation($appleSchoolLocation) {
        $this->appleSchoolLocation = $appleSchoolLocation;
    }

    /**
     * Serializer\Exclude()
     */
    public static $settingsFieldNulled = [
        'autoCreateUsername' => null,
        'forceImportedPassword' => null,
        'forceImportedPasswordInitial' => null,
        'ldapCreateOU' => null,
        'ldapOUPrefix' => null,
        'ldapOUPath' => null,
        'googleAppDomain' => null,
        'googleAppClientId' => null,
        'googleAppClientSecret' => null,
        'googleAppAutoEmail' => null,
        'googleAppAutoEmailStyle' => null,
        'googleAppCreateGroup' => null,
        'googleAppCreateProviderGroup' => null,
        'googleAppGroupPrefix' => null,
        'googleAppGroupExtraEmail' => null,
        'googleAppProviderGroupExtraEmail' => null,
        'googleAppUseUserInProviderGroup' => null,
        'googleAppCreateOU' => null,
        'googleAppOUPrefix' => null,
        'googleAppOUPath' => null,
    ];

    public function getStudentLdapCreateOU(): ?bool
    {
        return $this->studentLdapCreateOU;
    }

    public function getStudentGoogleAppCreateProviderGroup(): ?bool
    {
        return $this->studentGoogleAppCreateProviderGroup;
    }

    public function getStudentGoogleAppUseUserInProviderGroup(): ?bool
    {
        return $this->studentGoogleAppUseUserInProviderGroup;
    }

    public function getTeacherLdapCreateOU(): ?bool
    {
        return $this->teacherLdapCreateOU;
    }

    public function getTeacherGoogleAppCreateProviderGroup(): ?bool
    {
        return $this->teacherGoogleAppCreateProviderGroup;
    }

    public function getTeacherGoogleAppUseUserInProviderGroup(): ?bool
    {
        return $this->teacherGoogleAppUseUserInProviderGroup;
    }

    public function addSector(Sector $sector): self
    {
        if (!$this->sectors->contains($sector)) {
            $this->sectors[] = $sector;
            $sector->setProvider($this);
        }

        return $this;
    }

    public function removeSector(Sector $sector): self
    {
        if ($this->sectors->contains($sector)) {
            $this->sectors->removeElement($sector);
            // set the owning side to null (unless already changed)
            if ($sector->getProvider() === $this) {
                $sector->setProvider(null);
            }
        }

        return $this;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
            $subject->setProvider($this);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
            // set the owning side to null (unless already changed)
            if ($subject->getProvider() === $this) {
                $subject->setProvider(null);
            }
        }

        return $this;
    }

    public function addTeacher(Teacher $teacher): self
    {
        if (!$this->teachers->contains($teacher)) {
            $this->teachers[] = $teacher;
            $teacher->setProvider($this);
        }

        return $this;
    }

    public function removeTeacher(Teacher $teacher): self
    {
        if ($this->teachers->contains($teacher)) {
            $this->teachers->removeElement($teacher);
            // set the owning side to null (unless already changed)
            if ($teacher->getProvider() === $this) {
                $teacher->setProvider(null);
            }
        }

        return $this;
    }

//    public function addAdditionalStudent(Student $additionalStudent): self
//    {
//        if (!$this->additionalStudents->contains($additionalStudent)) {
//            $this->additionalStudents[] = $additionalStudent;
//            $additionalStudent->addAdditionalProvider($this);
//        }
//
//        return $this;
//    }
//
//    public function removeAdditionalStudent(Student $additionalStudent): self
//    {
//        if ($this->additionalStudents->contains($additionalStudent)) {
//            $this->additionalStudents->removeElement($additionalStudent);
//            $additionalStudent->removeAdditionalProvider($this);
//        }
//
//        return $this;
//    }
//
//    public function addAdditionalTeacher(Teacher $additionalTeacher): self
//    {
//        if (!$this->additionalTeachers->contains($additionalTeacher)) {
//            $this->additionalTeachers[] = $additionalTeacher;
//            $additionalTeacher->addAdditionalProvider($this);
//        }
//
//        return $this;
//    }
//
//    public function removeAdditionalTeacher(Teacher $additionalTeacher): self
//    {
//        if ($this->additionalTeachers->contains($additionalTeacher)) {
//            $this->additionalTeachers->removeElement($additionalTeacher);
//            $additionalTeacher->removeAdditionalProvider($this);
//        }
//
//        return $this;
//    }

    public function addTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if (!$this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups[] = $teacherSubjectGroup;
            $teacherSubjectGroup->setProvider($this);
        }

        return $this;
    }

    public function removeTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if ($this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups->removeElement($teacherSubjectGroup);
            // set the owning side to null (unless already changed)
            if ($teacherSubjectGroup->getProvider() === $this) {
                $teacherSubjectGroup->setProvider(null);
            }
        }

        return $this;
    }


    public function getStudentForceImportedPasswordInitial(): bool
    {
        return $this->studentForceImportedPasswordInitial??false;
    }


    public function setStudentForceImportedPasswordInitial(bool $studentForceImportedPasswordInitial)
    {
        $this->studentForceImportedPasswordInitial = $studentForceImportedPasswordInitial;
    }


    public function getTeacherForceImportedPasswordInitial(): bool
    {
        return $this->teacherForceImportedPasswordInitial??false;
    }

    /**
     * @param bool $teacherForceImportedPasswordInitial
     */
    public function setTeacherForceImportedPasswordInitial(bool $teacherForceImportedPasswordInitial): void
    {
        $this->teacherForceImportedPasswordInitial = $teacherForceImportedPasswordInitial;
    }



}
