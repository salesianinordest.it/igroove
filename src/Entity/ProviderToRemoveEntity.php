<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 */
class ProviderToRemoveEntity {
	/**
	 * @var string
	 *
	 * @ORM\Column(type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;

	/**
	 * @var Provider
	 *
	 * @ORM\ManyToOne(targetEntity="Provider", inversedBy="groups")
	 * Serializer\MaxDepth(2)
	 **/
	private $provider;

	/**
	 * @var string
	 * @ORM\Column(name="entity_type", type="string", nullable=true)
	 */
	private $entityType;

	/**
	 * @var string
	 * @ORM\Column(name="entity_id", type="string", nullable=true)
	 */
	private $entityId;


	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @return Provider
	 */
	public function getProvider(): Provider {
		return $this->provider;
	}

	/**
	 * @param Provider $provider
	 * @return ProviderToRemoveEntity
	 */
	public function setProvider(Provider $provider): ProviderToRemoveEntity {
		$this->provider = $provider;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEntityType(): string {
		return $this->entityType;
	}

	/**
	 * @param string $entityType
	 * @return ProviderToRemoveEntity
	 */
	public function setEntityType(string $entityType): ProviderToRemoveEntity {
		$this->entityType = $entityType;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEntityId(): string {
		return $this->entityId;
	}

	/**
	 * @param string $entityId
	 * @return ProviderToRemoveEntity
	 */
	public function setEntityId(string $entityId): ProviderToRemoveEntity {
		$this->entityId = $entityId;

		return $this;
	}
}