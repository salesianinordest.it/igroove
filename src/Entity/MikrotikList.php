<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class MikrotikList
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nome;

    /**
     *
     * @ORM\OneToMany(targetEntity="Device", mappedBy="mikrotikList")
     **/
    private $macs;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Mikrotik", inversedBy="ipLists")
     **/
    private $mikrotik;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->macs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->nome;
    }


    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return MikrotikList
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Add mac
     *
     * @param \App\Entity\Device $mac
     *
     * @return MikrotikList
     */
    public function addMac(\App\Entity\Device $mac)
    {
        $this->macs[] = $mac;

        return $this;
    }

    /**
     * Remove mac
     *
     * @param \App\Entity\Device $mac
     */
    public function removeMac(\App\Entity\Device $mac)
    {
        $this->macs->removeElement($mac);
    }

    /**
     * Get macs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMacs()
    {
        return $this->macs;
    }

    /**
     * Set mikrotik
     *
     * @param \App\Entity\Mikrotik $mikrotik
     *
     * @return MikrotikList
     */
    public function setMikrotik(\App\Entity\Mikrotik $mikrotik = null)
    {
        $this->mikrotik = $mikrotik;

        return $this;
    }

    /**
     * Get mikrotik
     *
     * @return \App\Entity\Mikrotik
     */
    public function getMikrotik()
    {
        return $this->mikrotik;
    }
}
