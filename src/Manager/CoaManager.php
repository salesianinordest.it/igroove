<?php

namespace App\Manager;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;

class CoaManager
{

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(ConfigurationManager $configurationManager, LoggerInterface $logger)
    {
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }


    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUser(string $username)
    {
        if (strlen($this->configurationManager->getCoaEndpoint()) > 0) {
            $client = new Client();
            $this->logger->info('CoA request: kickoff of ' . $username);
            $client->post(
                $this->configurationManager->getCoaEndpoint(),
                array(
                    'query' => array(
                        'pattern' => $this->configurationManager->getCoaPattern(),
                        'password' => $this->configurationManager->getCoaPassword(),
                        'username' => $username
                    )
                )
            );

        }
    }


    public function KickOffUsers(array $usernames){
        foreach ($usernames as $username){
            $this->KickOffUser($username);
        }
    }
}
