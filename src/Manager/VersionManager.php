<?php

namespace App\Manager;

use GuzzleHttp\Client;
use Symfony\Component\Yaml\Yaml;


class VersionManager
{

    private $versionFilesDir;

    public function __construct($kernelRootDir)
    {
        $this->versionFile = $kernelRootDir . '/../config/packages/igroove_version.yaml';
        $this->cachedDownloadFile = $kernelRootDir . '/../var/cache/igroove_version.yaml';
    }

    public function getVersions()
    {


        try {
            if (file_exists(       $this->versionFile ))
                $versionInstalled = Yaml::parse(file_get_contents(       $this->versionFile ));
            else {
                $versionInstalled = ['parameters'=>['igroove_version_installed'=>0]];
            }
        } catch (\Exception $e) {
        }
        try {
            if (file_exists(      $this->cachedDownloadFile ))
                $versionAvailable = Yaml::parse(file_get_contents(       $this->cachedDownloadFile ));
            else {
                $this->checkNewVersion();
                $versionAvailable = Yaml::parse(file_get_contents(       $this->cachedDownloadFile ));
            }
        } catch (\Exception $e) {
        }

        return array(
      'versionInstalled'=>      $versionInstalled['parameters']['versionInstalled'],
      'versionAvailable'=>      $versionAvailable['parameters']['versionInstalled']
    );
    }

    public function checkNewVersion()
    {
        $guzzleClient = new Client();
        try {
            $response = $guzzleClient->request('get','https://gitlab.com/consorzisdb/igroove/raw/master/config/packages/igroove_version.yaml');
            $versionAvailable = trim((string)$response->getBody());
            file_put_contents($this->cachedDownloadFile, $versionAvailable);
        } catch (BadResponseException $e) {
            echo "\r\nOps Error getting new version";
        }
    }


}
