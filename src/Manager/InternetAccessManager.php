<?php

namespace App\Manager;

use App\LdapTool;
use App\MikrotikPool;
use Doctrine\ORM\EntityManager;

class InternetAccessManager
{
    protected $mikrotikPool;
    protected $groupNameInInternet;
    protected $userNameInInternet;
    protected $em;
    protected $configurationAD;
    protected $adldapManager;
    protected $coaManager;

    public function __construct(ConfigurationManager $configurationManager, $adldapManager, MikrotikManager $mikrotikManager, EntityManager $em, CoaManager $coaManager)
    {

        $this->configurationAD = $configurationManager->getActiveDirectoryConfiguration();
        $this->adldapManager = $adldapManager;

        $this->mikrotikManager = $mikrotikManager;
        $this->coaManager = $coaManager;
        $this->getUsers();
        $this->em = $em;
    }

    private function getUsers()
    {


        $adldap = $this->adldapManager->getAdLdap();
        $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        $groupNameInInternet = array();
        if (!is_array($groupsInInternet)) {
            sleep(10);
            $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        }
        if (is_array($groupsInInternet)) {
            foreach ($groupsInInternet as $key => $groupAD) {
                $groupNameInInternet[] = trim(substr($groupAD, 3, strpos($groupAD, ',', 3) - 3));
            }
        }


        $this->userNameInInternet = $adldap->group()->members('_InternetAccess', false);

        $this->groupNameInInternet = $groupNameInInternet;

    }

    public function addGroup($group)
    {
        $adldap = $this->adldapManager->getAdLdap();

        if (!in_array($group, $this->groupNameInInternet)) {
            $result = $adldap->group()->addGroup('_InternetAccess', $group);
            if ($result) {
                $this->getUsers();
                $this->adldapManager->explodeGroupInternetAccess();
            }
        }
    }

    public function removeGroup($group)
    {
        $adldap = $this->adldapManager->getAdLdap();


        if (in_array($group, $this->groupNameInInternet)) {
            $result = $adldap->group()->removeGroup('_InternetAccess', $group);
            if ($result) {
                $entities = $this->em->getRepository('App:InternetOpen')->findBy(
                    array('type' => 'group', 'account' => strtolower($group))
                );
                foreach ($entities as $entity) {


                    $this->em->remove($entity);
                    $this->em->flush();
                    $this->removeCoaFromGroup($entity->getAccount());

                }

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
                $this->mikrotikManager->KickOffUsers($this->adldapManager);

            }
        }

    }

    public function addUser($user)
    {
        $adldap = $this->adldapManager->getAdLdap();
        if (!in_array($user, $this->userNameInInternet)) {
            $result = $adldap->group()->addUser('_InternetAccess', $user);
            if ($result) {
                $this->getUsers();
                $this->adldapManager->explodeGroupInternetAccess();
            }
        }
    }

    public function removeUser($user)
    {
        $adldap = $this->adldapManager->getAdLdap();
        if (in_array($user, $this->userNameInInternet)) {
            $result = $adldap->group()->removeUser('_InternetAccess', $user);
            if ($result) {
                $entities = $this->em->getRepository('App:InternetOpen')->findBy(
                    array('type' => 'user', 'account' => strtolower($user))
                );
                foreach ($entities as $entity) {
                    $this->em->remove($entity);
                    $this->em->flush();
                    $this->coaManager->KickOffUser($entity->getAccount());
                }

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
                $this->mikrotikManager->KickOffUsers($this->adldapManager);
            }
        }
    }


    private function removeCoaFromGroup($groupName)
    {
        $usernames = $this->em->getRepository('App:LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
            $this->coaManager->KickOffUser($username);
        }
    }


}
