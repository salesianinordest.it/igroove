<?php

namespace App\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Psr\Log\LoggerInterface;
use RouterOS\Client;
use RouterOS\Query;

class MikrotikManager
{
    protected $mikrotiks;
    /**
     * @var Client[]
     */
    protected $mikrotikClient = [];
    protected $userIntoMikrotik;
    protected $internetAccessUsers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $connected = false;
    /**
     * @var EntityManager
     */
    protected $em;

    static $bypassedIpListName = "Bypassed Ip";

    public function __construct(ConfigurationManager $configurationManager, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }


    private function connect()
    {
        if (!$this->connected) {
            $this->loadClientsHandler();
            $this->connected = true;
        }
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUsers()
    {
        $this->connect();
        $this->internetAccessUsers = $groupMembersUsers = $this->em->getRepository('App:LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix() . 'InternetAccess');
        $userList = array();
        foreach ($groupMembersUsers as $user) {
            $userList[] = strtolower($user);
        }

        $igrooveUsers = $this->em->getRepository('App:Student')->getAllStudentsUsername();

        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            $userIntoMikrotik = $this->getUsersInHotspot($mikrotikIp);
            $usernameToRemove = array_diff(
                array_unique(array_keys($userIntoMikrotik)),
                array_unique($userList)
            );

            foreach ($usernameToRemove as $user) {
                if (strlen(trim($user)) == 0) {
                    continue;
                }
                if (!array_search($user, $igrooveUsers)) {
                    continue;
                }
                if (array_key_exists(strtolower($user), $userIntoMikrotik)) {
                    echo "\r\n --> Mikrotik kickoff " . $user;
                    $this->logger->info("Mikrotik kickoff $user");
                    $delRequest = new Query('/ip/hotspot/active/remove', ['=numbers=' . $userIntoMikrotik[strtolower($user)]]);
                    $mtResult = $mikrotikClient->query($delRequest)->read();
                    if (isset($mtResult['after']['message'])) {
                        $this->logger->error('Error kickoff ' . $user . ': ' . $mtResult['after']['message']);
                    }
                }
            }
        }
    }

    /**
     * add or remove the mac address bypassed in the hotspot of all the mikrotiks.
     *
     * @return array action executed
     */
    public function manageMacToHotspot()
    {
        $this->connect();
        $criteria = array('active' => true, 'bypassHotspot' => true);
        $entities = $this->em->getRepository('App:Device')->findBy($criteria);
        $macInIgroove = array();
        foreach ($entities as $entity) {
            $macInIgroove[strtoupper($entity->getMac())] = 'igroove - ' . $entity->getDevice();
        }
        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (array_key_exists($mikrotikIp, $this->mikrotiks) == false) {
                continue;
            }
            if ($this->mikrotiks[$mikrotikIp]->getUseBypass() == false) {
                $result[$mikrotikIp]['ROUTER'] = 'skip this router bypass';
                continue;
            }
            $macIntoMikrotik = $this->getMacsIntoHotspot($mikrotikIp);
            $macToAdd = array_diff_key($macInIgroove, $macIntoMikrotik);
            $macToRemove = array_diff_key($macIntoMikrotik, $macInIgroove);

            foreach ($macToAdd as $mac => $comment) {
                $result[$mikrotikIp][$mac] = 'Add ' . $comment;
                $this->logger->info('Add Mikrotik bypass ' . $comment);
                $addRequest = new Query('/ip/hotspot/ip-binding/add',
                    [
                        '=mac-address=' . $mac,
                        '=type=bypassed',
                        '=comment=' . $comment
                    ]);
                $mtResult = $mikrotikClient->query($addRequest)->read();
                if (isset($mtResult['after']['message'])) {
                    $this->logger->error('Error adding ' . $comment . ': ' . $mtResult['after']['message']);
                }
            }
            foreach ($macToRemove as $mac => $element) {
                $result[$mikrotikIp][$mac] = 'Remove ' . $element['comment'];
                $delRequest = new Query('/ip/hotspot/ip-binding/remove', ['=numbers=' . $element['id']]);
                $this->logger->info('Remove Mikrotik bypass ' . $element['comment']);
                $mtResult = $mikrotikClient->query($delRequest)->read();
                if (isset($mtResult['after']['message'])) {
                    $this->logger->error('Error removing ' . $element['comment'] . ': ' . $mtResult['after']['message']);
                }
            }
        }
        return $result;
    }

    /**
     * add or remove the ip addresses in the firewall addresslist.
     *
     * @return array action executed
     */
    public function addIpToIpList()
    {
        $this->connect();
        $ipsInIgroove = $this->getIgrooveIpInAddressList();

        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (array_key_exists($mikrotikIp, $ipsInIgroove) == false) {
                continue;
            }

            $ipsIntoMikrotik = $this->getMikrotikIpAddresslists($mikrotikIp);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipsIntoMikrotik);
            $entriesToRemove = array_diff_key($ipsIntoMikrotik, $ipsInIgroove[$mikrotikIp]);

            foreach ($entriesToAdd as $k => $entry) {
                $result[$mikrotikIp][$k] = 'Add ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . $entry['list'];

                $comment = 'igroove - ' . $entry['device'];
                $addRequest = new Query('/ip/firewall/address-list/add', [
                    '=address=' . $entry['ip'],
                    '=list=' . $entry['list'],
                    '=comment=' . $comment
                ]);
                $mikrotikClient->query($addRequest);
            }

            foreach ($entriesToRemove as $k => $element) {
                $comment = $element['comment'];
                $result[$mikrotikIp][$k] = 'Remove ' . $comment;

                $delRequest = new Query('/ip/firewall/address-list/remove', [
                    '=numbers=' . $element['id']
                ]);
                $mikrotikClient->sendSync($delRequest);
            }
        }

        return $result;
    }

    public function addIpToBypassedList()
    {
        $this->connect();
        $ipsInIgroove = $this->getBypassedIgrooveIp();

        $result = [];
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (!isset($ipsInIgroove[$mikrotikIp])) {
                continue;
            }

            $ipInMikrotik = $this->getMikrotikIpInAddressList($mikrotikIp, self::$bypassedIpListName);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipInMikrotik);
            $entriesToRemove = array_diff_key($ipInMikrotik, $ipsInIgroove[$mikrotikIp]);

            foreach ($entriesToAdd as $k => $entry) {
                $result[$mikrotikIp][$k] = 'Add ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . self::$bypassedIpListName;

                $comment = 'igroove - ' . $entry['device'];
                $addRequest = new Query('/ip/firewall/address-list/add', [
                    '=address=' . $entry['ip'],
                    '=list=' . self::$bypassedIpListName,
                    '=comment=' . $comment
                ]);
                $mikrotikClient->query($addRequest);
            }

            foreach ($entriesToRemove as $k => $element) {
                $comment = $element['comment'];
                $result[$mikrotikIp][$k] = 'Remove ' . $comment;

                $delRequest = new Query('/ip/firewall/address-list/remove', [
                    '=numbers=' . $element['id']
                ]);
                $mikrotikClient->query($delRequest);
            }
        }

        $internetOpens = $this->em->getRepository('App:InternetOpen')->findBy(['type' => 'ipList', 'activationCompleated' => false]);
        foreach ($internetOpens as $internetOpen) {
            $internetOpen->setActivationCompleated(true);
        }
        $this->em->flush();

        return $result;
    }

    /**
     * add or remove pools with creareSuMikrotik option enabled on the mikrotiks.
     *
     * @return array
     */
    public function addPools()
    {
        $this->connect();
        $responses = array();
        foreach ($this->mikrotiks as $mikrotik) {
            try {
                $mikrotikClient = $this->getMikrotikClient($mikrotik->getIp());
            } catch (\ErrorException $e) {
                continue;
            }

            $mikrotikPools = $this->getMikrotikPools($mikrotik->getIp());
            $criteria = array('mikrotik' => $mikrotik, 'creareSuMikrotik' => true);
            $poolsInIgroove = $this->em->getRepository('App:MikrotikPool')->findBy($criteria);

            foreach ($poolsInIgroove as $poolInIgroove) {
                $rangesInIgroove = $poolInIgroove->getDottedIpStart() . '-' . $poolInIgroove->getDottedIpEnd();

                if (array_key_exists($poolInIgroove->getNome(), $mikrotikPools)) {
                    $rangesInMikrotik = $mikrotikPools[$poolInIgroove->getNome()]['ranges'];
                    if ($rangesInIgroove == $rangesInMikrotik) {
                        continue;
                    }
                    $delRequest = new Query('/ip/pool/remove', [
                        '=numbers=' . $mikrotikPools[$poolInIgroove->getNome()]['id']
                    ]);
                    $mikrotikClient->query($delRequest);
                    $responses[$mikrotik->getIp()][] = 'DEL ' . $poolInIgroove->getNome() . ': ' . $rangesInMikrotik;
                }

                $addRequest = new Query('/ip/pool/add', [
                    '=name=' . $poolInIgroove->getNome(),
                    '=ranges=' . $rangesInIgroove
                ]);
                $mikrotikClient->query($addRequest);
                $responses[$mikrotik->getIp()][] = 'ADD ' . $poolInIgroove->getNome() . ': ' . $rangesInIgroove;
            }
        }

        return $responses;
    }

    /**
     * Add ip reservation in dhcp server lease.
     */
    public function addIpReservation()
    {
        $this->connect();
        $ipsInIgroove = $this->getIgrooveDeviceIps();

        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            $ipsInMikrotik = $ipsToRemoveFromMikrotik = $this->getMikrotikIpLeasesInDhcpServer($mikrotikIp);
            $ipsToAddToMikrotik = [];

            foreach ($ipsInIgroove[$mikrotikIp] as $deviceIpInIgroove => $ipInIgroove) {
                if (array_key_exists($deviceIpInIgroove, $ipsInMikrotik) == false) {
                    if (sizeof($ipInIgroove['ip']) > 0) {
                        $ipsToAddToMikrotik[] = $ipInIgroove;
                    }
                    continue;
                }

                if ($ipsInMikrotik[$deviceIpInIgroove]['check_string'] != $ipInIgroove['check_string']) {
                    $updRequest = new Query('/ip/dhcp-server/lease/set', [
                        '=numbers=' . $ipsInMikrotik[$deviceIpInIgroove]['id'],
                        '=comment=' . 'igroove - ' . $ipInIgroove['name'],
                        '=mac-address=' . $ipInIgroove['mac'],
                        '=address=' . $ipInIgroove['ip'],
                        '=server=' . $ipInIgroove['server']
                    ]);
                    $mikrotikClient->query($updRequest);

                    $result[$mikrotikIp][$deviceIpInIgroove] = 'Update ' . $ipInIgroove['name'] . ' (' . $ipInIgroove['ip'] . ') in dhcp server ' . $ipInIgroove['server'] . " | {$ipsInMikrotik[$deviceIpInIgroove]['check_string']} {$ipInIgroove['check_string']}";
                }

                unset($ipsToRemoveFromMikrotik[$deviceIpInIgroove]);
            }

            foreach ($ipsToRemoveFromMikrotik as $ipToRemove => $ipToRemoveFromMikrotik) {
                $delRequest = new Query('/ip/dhcp-server/lease/remove', [
                    '=numbers=' . $ipToRemoveFromMikrotik['id']
                ]);
                $mikrotikClient->query($delRequest);

                $result[$mikrotikIp][$ipToRemove] = 'Remove ' . $ipToRemoveFromMikrotik['name'] . ' (' . $ipToRemoveFromMikrotik['ip'] . ' - ' . $ipToRemoveFromMikrotik['mac'] . ') to dhcp server ' . $ipToRemoveFromMikrotik['server'];
            }

            foreach ($ipsToAddToMikrotik as $ipToAddToMikrotik) {
                $result[$mikrotikIp][$ipToAddToMikrotik['ip']] = 'Add ' . $ipToAddToMikrotik['name'] . ' (' . $ipToAddToMikrotik['ip'] . ') to dhcp server ' . $ipToAddToMikrotik['server'];

                $addRequest = new Query('/ip/dhcp-server/lease/add', [
                    '=comment=' . 'igroove - ' . $ipToAddToMikrotik['name'],
                    '=mac-address=' . $ipToAddToMikrotik['mac'],
                    '=address=' . $ipToAddToMikrotik['ip'],
                    '=server=' . $ipToAddToMikrotik['server']
                ]);
                $mikrotikClient->query($addRequest);
            }
        }

        return $result;
    }

    /**
     * Get Current active users in hotspot of mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getUsersInHotspot($mikrotikIp)
    {
        $userIntoMikrotik = array();

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $userIntoMikrotik;
        }

        $responses = $mikrotikClient->query(new Query('/ip/hotspot/active/print'))->read();
        $userIntoMikrotik = array();

        foreach ($responses as $element) {
            $username = explode('@', $element['user']);
            if (empty($username) || $username[0] == '') {
                continue;
            }

            /*
            if (isset($username[1]) && strtolower($this->configurationManager->getFreeradiusRealm()) != strtolower($username[1])) {
                continue;
            } elseif (!isset($username[1]) && $this->configurationManager->getFreeradiusRealm() != '') {
                continue;
            }
*/
            $userIntoMikrotik[strtolower($username[0])] = $element['.id'];
        }

        return $userIntoMikrotik;
    }

    /**
     * Get Current active users in all the mikrotiks hotspot.
     *
     * @return array
     */
    public function getAllUsersInHotspots()
    {
        $this->connect();
        $userIntoMikrotik = array();
        foreach ($this->mikrotikClient as $mikrotikClient) {
            $responses = $mikrotikClient->query(
                '/ip/hotspot/active/print'
            )->read();

            foreach ($responses as $element) {
                $userIntoMikrotik[strtolower($element['user'])] = strtolower($element['server']);
            }
        }

        return $userIntoMikrotik;
    }

    /**
     * @param $mikrotikIp
     *
     * @return array
     */
    public function getDhcpServersName($mikrotikIp)
    {
        $this->connect();
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/dhcp-server/print')->read();
        $dhcpServersName = array();
        foreach ($responses as $element) {
            if ($element['name'] == '') {
                continue;
            }

            $dhcpServersName[] = $element['name'];
        }

        return $dhcpServersName;
    }

    /**
     * get the mac addresses of the devices bypassed in the hotspot of the mikrotik with ip specified.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMacsIntoHotspot($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/hotspot/ip-binding/print')->read();
        $macIntoMikrotik = array();
        foreach ($responses as $element) {
            if (isset($element['comment']) && substr($element['comment'], 0, 7) == 'igroove') {
                $macIntoMikrotik[strtoupper($element['mac-address'])] = array(
                    'id' => $element['.id'],
                    'comment' => $element['comment'],
                );
            }
        }
        return $macIntoMikrotik;
    }

    /**
     * get the list of ips in all the addresslist of the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpAddresslists($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/firewall/address-list/print')->read();
        $ipAddresslistIntoMikrotik = array();
        foreach ($responses as $element) {
            if (isset($element['comment']) && substr($element['comment'], 0, 7) == 'igroove' && $element['list'] != self::$bypassedIpListName) {
                $ipAddresslistIntoMikrotik[md5(strtolower($element['address'] . $element['list']))] = array(
                    'id' => $element['.id'],
                    'address' => $element['address'],
                    'list' => $element['list'],
                    'comment' => $element['comment'],
                );
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    private function getMikrotikIpInAddressList($mikrotikIp, $listName)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $request = new Query('/ip/firewall/address-list/print');
        $request->setQuery(Query::where('list', $listName));
        $responses = $mikrotikClient->sendSync($request);
        $ipAddresslistIntoMikrotik = array();
        foreach ($responses as $element) {
            if (isset($element['comment']) && substr($element['comment'], 0, 7) == 'igroove') {
                $ipAddresslistIntoMikrotik[md5($element['address'])] = array(
                    'id' => $element['.id'],
                    'address' => $element['address'],
                    'comment' => $element['comment'],
                );
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    /**
     * Get all ips assigned to an address list in igroove.
     *
     * @return array
     */
    private function getIgrooveIpInAddressList()
    {
        $query = $this->em->getRepository('App:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,l.nome,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('App:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp . strtolower($result['nome']))] = array(
                    'ip' => $deviceIp,
                    'list' => $result['nome'],
                    'device' => $result['device'],
                );
            }
        }

        return $ipInAddresslist;
    }

    private function getBypassedIgrooveIp()
    {
        $query = $this->em->getRepository('App:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->join('App\Entity\InternetOpen', 'io', Join::WITH, 'l.id = io.account')
            ->where("d.active = :true AND io.type = 'iplist' AND io.close_at > :now")
            ->setParameter('true', true)
            ->setParameter('now', new \DateTime('now'))
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('App:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp)] = array(
                    'ip' => $deviceIp,
                    'device' => $result['device'],
                );
            }
        }

        return $ipInAddresslist;
    }

    /**
     * Get all ip currently set from igroove in dhcp server as lease.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpLeasesInDhcpServer($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/dhcp-server/lease/print')->read();
        $ipIntoMikrotik = array();
        foreach ($responses as $element) {
            if (isset($element['comment']) && substr($element['comment'], 0, 7) == 'igroove') {
                $name = substr($element['comment'], 10);
                $mac = strtoupper($element['mac-address']);
                $ipIntoMikrotik[$element['address']] = array(
                    'id' => $element['.id'],
                    'ip' => $element['address'],
                    'name' => $name,
                    'mac' => $mac,
                    'server' => $element['server'],
                    'check_string' => md5(strtolower($element['address'] . $name . $element['mac-address'] . $element['server'])),
                );
            }
        }

        return $ipIntoMikrotik;
    }

    /**
     * get the list of device ips currently in igroove.
     *
     * @return array
     */
    private function getIgrooveDeviceIps()
    {
        $query = $this->em->getRepository('App:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,d.mac,p.dhcpServerName,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('i.mikrotikPool', 'p')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = p.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();
        $mts = $this->em->getRepository('App:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInIgroove[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $mac = strtoupper($result['mac']);
                $ipInIgroove[$result['mip']][$deviceIp] = array(
                    'ip' => $deviceIp,
                    'name' => $result['device'],
                    'mac' => $mac,
                    'server' => $result['dhcpServerName'],
                    'check_string' => md5(strtolower($deviceIp . $result['device'] . $result['mac'] . $result['dhcpServerName'])),
                );
            }
        }

        return $ipInIgroove;
    }

    /**
     * get the dhcp pools in the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikPools($mikrotikIp)
    {
        $poolsIntoMikrotik = array();

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $poolsIntoMikrotik;
        }

        $responses = $mikrotikClient->query('/ip/pool/print')->read();
        foreach ($responses as $element) {
            if ($element['name']) {
                $poolsIntoMikrotik[($element['name'])] = array(
                    'id' => $element['.id'],
                    'name' => $element['name'],
                    'ranges' => $element['ranges'],
                    'comment' => $element['comment'],
                );
            }
        }

        return $poolsIntoMikrotik;
    }

    /**
     * reboot all the mikrotiks.
     *
     * @return array
     */
    public function rebootMikrotiks()
    {
        $this->connect();
        $messages = array();
        foreach ($this->mikrotikClient as $ip => $mikrotikClient) {
            $rebootRequest = new Query('/system/reboot');
            $rebootRequest->setTag('rebootFromIgroove');
            $mikrotikClient->sendAsync($rebootRequest);
            $messages[] = 'Reboot ' . $ip;
        }

        return $messages;
    }

    /**
     * load the mikrotiks client.
     */
    protected function loadClientsHandler()
    {
        $mts = $this->em->getRepository('App:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $this->mikrotiks[$m->getIp()] = $m;
        }
        $this->mikrotikClient = array();
        if ($this->mikrotiks) {
            foreach ($this->mikrotiks as $mikrotik) {
                try {
                    $this->mikrotikClient[$mikrotik->getIp()] = new Client([
                            'host' => $mikrotik->getIp(),
                            'user' => $mikrotik->getApiUsername(),
                            'pass' => $mikrotik->getApiPassword()
                        ]
                    );
                    $mikrotik->setStatus(true);
                } catch (\Exception $e) {
                    unset($this->mikrotikClient[$mikrotik->getIp()]);
                    $mikrotik->setStatus(false);
                    continue;
                }
            }
            $this->em->flush();
        }

    }

    /**
     * @param $mikrotikIp
     *
     * @return Ros
     *
     * @throws \ErrorException
     */
    protected function getMikrotikClient($mikrotikIp)
    {
        if ((array_key_exists($mikrotikIp, $this->mikrotikClient) == false) || !$this->mikrotikClient[$mikrotikIp] instanceof Client) {
            throw new \ErrorException("Mikrotik with ip {$mikrotikIp} not accessible");
        }

        return $this->mikrotikClient[$mikrotikIp];
    }

    /**
     * @return Ros[]
     */
    protected function getMikrotikClients()
    {
        return $this->mikrotikClient;
    }

    protected function checkSyncKey($stringToCheck)
    {

    }
}
