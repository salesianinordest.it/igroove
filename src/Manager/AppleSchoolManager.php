<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use \App\Entity\AppleSchool;
use \App\Entity\Provider;
use \App\Entity\LdapUser;

class AppleSchoolManager {

	protected $staffInserted = [];
	protected $subjectsPerClassroom = [];

	public function __construct(EntityManagerInterface $em, $baseDirectory, Logger $logger, $inDebug)
	{
		$this->em = $em;
		$this->baseDirectory = $baseDirectory;
		$this->logger = $logger;
		$this->inDebug = $inDebug;
	}

	public function generateAndUploadAllCsvFiles() {
		$this->logger->info("[appleSchool] Start generating & uploading all the csv");

		$appleSchools = $this->em->getRepository('App:AppleSchool')->findAll();
		foreach ($appleSchools as $appleSchool) {
			try {
				$this->generateAndUploadCsvFilesFor($appleSchool);
			} catch (\Exception $e) {
				$this->logger->error("[appleSchool] Error during the generation/upload of the csv file for the org {$appleSchool->getOrganizationName()}: ".$e->getMessage());
				if($this->inDebug) {
					throw $e;
				}
			}
		}
	}

	/**
	 * @param AppleSchool $appleSchool
	 * @throws \Exception
	 */
	public function generateAndUploadCsvFilesFor(AppleSchool $appleSchool) {
		if(!$appleSchool->getActive()) {
			return;
		}

		$this->logger->info("[appleSchool] Generating the csv for the org {$appleSchool->getOrganizationName()}");
		$basePath = $this->baseDirectory.$appleSchool->getOrganizationId()."/";
		if((!is_dir($this->baseDirectory) && !mkdir($this->baseDirectory)) || (!is_dir($basePath) && !mkdir($basePath))) {
			throw new \Exception("Error creating the local cache directory");
		}

//		$providers = $appleSchool->getProviders();
		$this->generateLocationsCsvFile($appleSchool, $basePath."locations.csv");
		$this->generateStaffCsvFile($appleSchool, $basePath."staff.csv");
		$this->generateStudentsCsvFile($appleSchool, $basePath."students.csv");
		$this->generateCoursesAndClassesCsvFile($appleSchool, $basePath."courses.csv", $basePath."classes.csv");
//		$this->generateCoursesCsvFile($appleSchool, $providers, $basePath."courses.csv");
//		$this->generateClassesCsvFile($appleSchool, $providers, $basePath."classes.csv");
		$this->generateRostersCsvFile($appleSchool, $basePath."rosters.csv");

		$zipPath = $this->baseDirectory.$appleSchool->getOrganizationId().".zip";
		if(file_exists($zipPath) && !unlink($zipPath)) {
			throw new \Exception("Error deleting the previous zip file");
		}

		$zip = new \ZipArchive();
		if ($zip->open( $zipPath, \ZipArchive::CREATE)!==TRUE) {
			throw new \Exception("Error creating the zip file");
		}

		foreach (["locations.csv", "staff.csv", "students.csv", "courses.csv", "classes.csv", "rosters.csv"] as $filename) {
			$zip->addFile($basePath.$filename, $filename);
		}

		$zip->close();

		$connection = ssh2_connect('upload.appleschoolcontent.com', 22);
		if($connection === false || !ssh2_auth_password($connection, $appleSchool->getSftpUsername(), $appleSchool->getSftpPassword())) {
			throw new \Exception("Error connecting to the sftp");
		}

		$this->logger->info("[appleSchool] Uploading the csv for the org {$appleSchool->getOrganizationName()}");
		$sftp = ssh2_sftp($connection);
		$stream = @fopen("ssh2.sftp://{$sftp}/dropbox/{$appleSchool->getOrganizationId()}.zip", 'w');
		if (!$stream) {
			throw new \Exception("Impossible to create the sftp stream");
		}

		if (@fwrite($stream, @file_get_contents($zipPath)) === false) {
			throw new \Exception("Error writing to the sftp connection");
		}

		@fclose($stream);

		$this->logger->info("[appleSchool] Upload compleated for the org {$appleSchool->getOrganizationName()}");
	}

	public function generateLocationsCsvFile(AppleSchool $appleSchool, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['location_id', 'location_name']);
		foreach ($appleSchool->getLocations() as $location) {
			fputcsv($csvFile, [$location->getId(), $location->getName()]);
		}
		fclose($csvFile);
	}

	public function generateStaffCsvFile(AppleSchool $appleSchool, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['person_id', 'person_number', 'first_name', 'middle_name', 'last_name', 'email_address', 'sis_username', 'password_policy', 'location_id']);
		foreach ($appleSchool->getLocations() as $location) {
			foreach ($location->getProviders() as $provider) {
				/** @var Provider $provider */
				$teachers = $provider->getTeachers();
				foreach ($teachers as $teacher) {
					$ldapUser = $this->em->getRepository("ZenIgrooveBundle:LdapUser")->findOneBy(['username' =>$teacher->getUsername()]);
					if(!$ldapUser instanceof LdapUser) {
						continue;
					}

					$ldapAttributes = json_decode($ldapUser->getAttributes());
					if(!isset($ldapAttributes->objectGUID)) {
						continue;
					}

					$this->staffInserted[] = $teacher->getId();

					fputcsv($csvFile, [
						$teacher->getId(),
						$this->generateUniqueIdFromObjectGUID($ldapAttributes->objectGUID),
						$teacher->getFirstname(),
						'',
						$teacher->getLastname(),
						$teacher->getEmail(),
						$teacher->getUsername(),
						'',
						$location->getId()
					]);
				}
			}
		}
		fclose($csvFile);
	}

	public function generateStudentsCsvFile(AppleSchool $appleSchool, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['person_id', 'person_number', 'first_name', 'middle_name', 'last_name', 'grade_level', 'email_address', 'sis_username', 'password_policy', 'location_id']);
//		fputcsv($csvFile, ['fake', 'fake', 'fake', '', 'fake', '', '', '', '', '1']);
		foreach ($appleSchool->getLocations() as $location) {
			foreach ($location->getProviders() as $provider) {
				/** @var Provider $provider */
				$students = $provider->getStudents();
				foreach ($students as $student) {
					$ldapUser = $this->em->getRepository("ZenIgrooveBundle:LdapUser")->findOneBy(['username' =>$student->getUsername()]);
					if(!$ldapUser instanceof LdapUser) {
						continue;
					}

					$ldapAttributes = json_decode($ldapUser->getAttributes());
					if(!isset($ldapAttributes->objectGUID)) {
						continue;
					}

					fputcsv($csvFile, [
						$student->getId(),
						$this->generateUniqueIdFromObjectGUID($ldapAttributes->objectGUID),
						$student->getFirstname(),
						'',
						$student->getLastname(),
						'',
						$student->getEmail(),
						$student->getUsername(),
						'',
						$location->getId()
					]);
				}
			}
		}

		fclose($csvFile);
	}

	public function generateCoursesAndClassesCsvFile(AppleSchool $appleSchool, $coursesFilePath, $classesFilePath) {
		$courseCsvFile = fopen($coursesFilePath, 'w');
		$classesCsvFile = fopen($classesFilePath, 'w');
		fputcsv($courseCsvFile, ['course_id', 'course_number', 'course_name', 'location_id']);
		fputcsv($classesCsvFile, ['class_id', 'class_number', 'course_id', 'instructor_id', 'instructor_id_2', 'instructor_id_3', 'location_id']);
		foreach ($appleSchool->getLocations() as $location) {
			foreach ($location->getProviders() as $provider) {
				/** @var Provider $provider */
				$classrooms = $this->em->getRepository("ZenIgrooveBundle:Group")->findBy(['provider' => $provider->getId(), 'manageManually' => false]);
				foreach ($classrooms as $classroom) {
					fputcsv($courseCsvFile, [$classroom->getId(), '', $classroom->getName(), $location->getId()]);

					if($appleSchool->getCourseType() === 0 || $appleSchool->getCourseType() === 2) {
						fputcsv($classesCsvFile, [$classroom->getId(), '', $classroom->getId(), '', '', '', $location->getId()]);
					}

					if($appleSchool->getCourseType() === 1 || $appleSchool->getCourseType() === 2) {
						$tsgPerSubjects = [];
						$this->subjectsPerClassroom[$classroom->getId()] = [];

						foreach($classroom->getTeacherSubjectGroups() as $tsg) {
							if($tsg->getTeacher()->getProvider()->getId() !== $provider->getId()) {
								continue;
							}

							if(!isset($tsgPerSubjects[$tsg->getSubject()->getId()])) {
								$tsgPerSubjects[$tsg->getSubject()->getId()] = ['subjectName' => $tsg->getSubject()->getName(), 'teachers' => []];
							}

							if(array_search($tsg->getTeacher()->getId(), $this->staffInserted) !== False) {
								$tsgPerSubjects[$tsg->getSubject()->getId()]['teachers'][] = $tsg->getTeacher()->getId();
							}
							$this->subjectsPerClassroom[$classroom->getId()][$tsg->getSubject()->getId()] = $tsg->getSubject()->getId();
						}

						foreach ($tsgPerSubjects as $subjectId => $tsgPerSubject) {
							fputcsv($classesCsvFile, [$classroom->getId()."-".$subjectId, $tsgPerSubject['subjectName'], $classroom->getId(),
								$tsgPerSubject['teachers'][0] ?? '', $tsgPerSubject['teachers'][1] ?? '', $tsgPerSubject['teachers'][2] ?? '', $location->getId()]);
						}
					}
				}
			}
		}

//		fputcsv($courseCsvFile, ['fake', 'fake', 'fake', '1']);
		fclose($courseCsvFile);
	}

	public function generateCoursesCsvFile(AppleSchool $appleSchool, $providers, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['course_id', 'course_number', 'course_name', 'location_id']);
		fputcsv($csvFile, ['fake', 'fake', 'fake', '1']);
		fclose($csvFile);
	}

	public function generateClassesCsvFile(AppleSchool $appleSchool, $providers, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['class_id', 'class_number', 'course_id', 'instructor_id', 'instructor_id_2', 'instructor_id_3', 'location_id']);
		fputcsv($csvFile, ['fake', 'fake', 'fake', '', '', '', '1']);
		fclose($csvFile);
	}

	public function generateRostersCsvFile(AppleSchool $appleSchool, $filePath) {
		$csvFile = fopen($filePath, 'w');
		fputcsv($csvFile, ['roster_id', 'class_id', 'student_id']);
		foreach ($appleSchool->getLocations() as $location) {
			foreach ($location->getProviders() as $provider) {
				/** @var Provider $provider */
				$students = $provider->getStudents();
				foreach ($students as $student) {
					$groups = $student->getNotManuallyManagedGroups();
					if ($groups->count() < 1) {
						continue;
					} elseif ($groups->count() === 1) {
						$class = $groups->first();
						if ($class->getProvider()->getId() !== $provider->getId()) {
							continue;
						}
					} else {
						$class = NULL;
						foreach ($groups as $group) {
							if ($group->getProvider()->getId() === $provider->getId()) {
								$class = $group;
								break;
							}
						}

						if ($class === NULL) {
							continue;
						}
					}

					if ($appleSchool->getCourseType() === 0 || $appleSchool->getCourseType() === 2) {
						fputcsv($csvFile, [$class->getId() . "-" . $student->getId(), $class->getId(), $student->getId()]);
					}

					if ($appleSchool->getCourseType() === 1 || $appleSchool->getCourseType() === 2) {
						if (isset($this->subjectsPerClassroom[$class->getId()])) {
							foreach ($this->subjectsPerClassroom[$class->getId()] as $subjectId) {
								fputcsv($csvFile, [$class->getId() . "-" . $student->getId() . "-" . $subjectId, $class->getId() . '-' . $subjectId, $student->getId()]);
							}
						}
					}
				}
			}
		}
//		fputcsv($csvFile, ['fake', 'fake', 'fake']);
		fclose($csvFile);
	}

	protected function generateUniqueIdFromObjectGUID($objectGUID) {
		switch(substr($objectGUID, 0, 1)) {
			case "A":
				$uniqueUidStr = 2;
				break;

			case "B":
				$uniqueUidStr = 3;
				break;

			case "C":
				$uniqueUidStr = 4;
				break;

			case "D":
				$uniqueUidStr = 5;
				break;

			case "E":
				$uniqueUidStr = 6;
				break;

			case "F":
				$uniqueUidStr = 7;
				break;

			case "9":
				$uniqueUidStr = 1;
				break;

			case "8":
				$uniqueUidStr = 0;
				break;

			default:
				$uniqueUidStr = substr($objectGUID, 0, 1);
		}

		$uniqueUidStr .= substr($objectGUID, 1, 7);

		return hexdec($uniqueUidStr);
	}
}