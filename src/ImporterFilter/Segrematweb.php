<?php

namespace App\ImporterFilter;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;
use GuzzleHttp\Client;


class Segrematweb extends AbstractFilter
{
    static $name = "Soluzione Srl - Segremat Web";
    static $internalName = "segrematweb";
    static $parametersUi = [
        'uri' => ['title' => "URI della fonte dati", 'type' => TextType::class],
        'secretKey' => ['title' => "Chiave Segreta", 'type' => TextType::class],
        'excludeGroups' => ['title' => "Classi da escludere nell'importazione (separati da virgola)", 'type' => TextType::class],
    ];

    private $dataUri;
    private $guzzle;

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);

        /*
         * Come concordato, per ottenere i dati occorre passare il parametro
         * k = md5((KEY + SYSDATE))
         * che funge da chiave di autenticazione.
         * Con
         * KEY     = chiave
         * SYSDATE = data attuale nel formato YYYYmmdd     (esempio per oggi 20141127)
         */

        $secretKey = md5($this->parameters['secretKey'] . date('Ymd'));
        $this->dataUri = $this->parameters['uri'] . "?Chiave=" . $secretKey;
    }

    public function parseRemoteData()
    {
        $remove = array(
            '^',
            ',',
            '.',
            ':',
            '/',
            '\\',
            ',',
            '=',
            '+',
            '<',
            '>',
            ';',
            '"',
            '#',
            "'",
            '(',
            ')',
            "'",
            "\x00",
            '?',
            '.',
            '-',
            '!',
            '°',
            '*'
        );
        try {
            $request = $this->guzzle->get($this->dataUri);

            $response = $request->getBody()->getContents();


            $body = utf8_encode(substr((string)$response, 1, -1));


            $listResult = json_decode($body);


            $list = $listResult->d->results;

        } catch (\Guzzle\Common\Exception\RuntimeException $e) {
            $list = array();
        }

        $genitori = array();
        foreach ($list as $k => $v) {

            $classe = trim($v->Classe);
            $classe = str_replace($remove, '', $classe);


            if ($this->skipThisClasse($classe)) {
                continue;
            }

            $idClasse = md5(strtolower($classe));


            if (trim(strtolower($v->CodiceAlunno)) == '') {
                continue;
            }
            if (strlen(trim($classe)) == 0) {
                continue;
            }

            $this->groups[$idClasse] = new Group($idClasse, $classe, 0);

            $id = $v->CodiceAlunno;
            $this->students[(int)$id] = new Student((int)$id, trim(strtolower($v->CodiceFiscale)), trim(ucwords(strtolower($v->Nome))), trim(ucwords(strtolower($v->Cognome))), $idClasse, trim(strtolower($v->email)));

            $gs = $v->Genitori;
            foreach ($gs as $g) {
                $e = $g->email;
                if (strlen($e) > 3) {
                    $genitori[$classe][] = $e;
                }
            }
        }
        foreach ($genitori as $classe => $elenco) {
            @mkdir('/var/www/igroove/var/elenco_genitori');
            @file_put_contents('/var/www/igroove/var/elenco_genitori/' . $classe . '.txt', implode("\r\n", $elenco));
        }
    }

    private function skipThisClasse($classe)
    {
        if (!isset($this->parameters['excludeGroups'])){return false;}
        if (strlen($this->parameters['excludeGroups'])==0){return false;}
        $excludeGroups = explode(",", $this->parameters['excludeGroups']);
        foreach ($excludeGroups as $group) {
            $group=trim(strtolower($group));
            $classe=trim(strtolower($classe));
            if (strlen($group)>strlen($classe)){return false;}
            if (substr($classe, 0, strlen($group)) == $group) {
                return true;
            }
        }
        return false;
    }

}
