<?php

namespace App\ImporterFilter;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;

class Mafol extends AbstractFilter
{
    static $name = "Mafol";
    static $internalName = "mafol";
    static $parametersUi = ['uri' => ['title' => "URI della fonte dati", 'type' => TextType::class],
        'secretKey' => ['title' => "Chiave Segreta", 'type' => TextType::class],
        'idSede' => ['title' => "Id Sede", 'type' => TextType::class],
        'emailDomain' => ['title' => "Considera solo utenti con email del dominio", 'type' => TextType::class]
    ];
    protected $uri;
    protected $secretKey;
    protected $idSede;
    protected $emailDomain;

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
    }

    public function parseRemoteData()
    {

        $token = md5($this->parameters['secretKey'] . date('ymd'));

        $client = new \SoapClient($this->parameters['uri'], array('trace' => 1));
        $parameters = array('idSede' => $this->parameters['idSede'], 'token' => $token);
        $response = $client->__soapCall('getAlunni', array($parameters));

        $resultArray = json_decode($response->getAlunniResult, true);
        if (sizeof($resultArray) < 3) {
            return;
        }

        foreach ($resultArray as $k => $v) {


            $remove = array(
                '^',
                ',',
                '.',
                ':',
                '/',
                '\\',
                ',',
                '=',
                '+',
                '<',
                '>',
                ';',
                '"',
                '#',
                "'",
                '(',
                ')',
                "'",
                "\x00",
                '?',
                '.',
                '-',
                '!',
                '°',
                '*'
            );
            $classe = trim($v['Group']);
            $classe = str_replace($remove, '', $classe);
            $idClasse = md5(strtolower($classe));


            if (trim(strtolower($v['id'])) == '') {
                continue;
            }
            if (strlen(trim($classe)) == 0) {
                continue;
            }
            $this->groups[$idClasse] = new Group($idClasse, $classe, 0);


            if (strpos($v['Email'], '@' . $this->parameters['emailDomain']) === false) {
                continue;
            }


            if (isset($v['Mafol-id'])) {
                $mafolId = $v['Mafol-id'];
		$username = substr($v['Email'], 0, strpos($v['Email'], '@'));

                $this->students[$mafolId] = new Student($mafolId, trim(strtolower($v['id'])), trim(ucwords(strtolower($v['FirstName']))), trim(ucwords(strtolower($v['LastName']))), $idClasse, trim(strtolower($v['Email'])), $username);
            }
        }
    }
}

