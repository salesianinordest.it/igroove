<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\Student;
use App\Form\StudentType;
use App\Manager\ConfigurationManager;
use App\Manager\PersonsAndGroups;
use Doctrine\Common\Annotations\AnnotationReader;
use Dompdf\Dompdf;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Knp\Component\Pager\PaginatorInterface;
use Ps\PdfBundle\Annotation\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
//use FOS\RestBundle\Controller\FOSRestController;
//use FOS\RestBundle\Controller\Annotations\Get;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Student controller.
 */
class StudentController extends BaseAbstractController
{
    /**
     * Lists all Student entities.
     *
     * @Route("/student", name="student_list")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction(PaginatorInterface $paginator, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $entities = $em->getRepository('App:Student')->findAll();

        $providers = $em->getRepository('App:Provider')->findBy([], ['name' => 'ASC']);

        $query = $em->getRepository('App:Student')->createQueryBuilder('s');
        $query->orderBy('s.lastname', 'ASC');

        if ($request->query->has('queryString') && '' != trim($request->query->get('queryString', ''))) {
            $query->where('s.firstname LIKE :qs OR s.lastname LIKE :qs OR s.username LIKE :qs OR s.email LIKE :qs')
                ->setParameter('qs', $request->query->get('queryString').'%');
        }

        if ($request->query->has('providerId') && $request->query->get('providerId') != "") {
            $query->andWhere('s.provider = :providerId')->setParameter('providerId', $request->query->get('providerId'));
        }

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

//        $students = [];
//        foreach ($pagination as $entity) {
//            $groupsName = [];
//            foreach ($entity->getGroups() as $group) {
//                $groupsName[] = $group->getName();
//            }
//            $students[] = [
//                'fiscalCode' => $entity->getFiscalCode(),
//                'firstname' => $entity->getFirstname(),
//                'lastname' => $entity->getLastname(),
//                'groups' => implode(", ", $groupsName),
//                'username' => $entity->getUsername(),
//                'startingPassword' => $entity->getStartingPassword(),
//                'email' => $entity->getEmail(),
//                'providerName' => $entity->getProvider()->getName(),
//            ];
//        }

//        $view = $this->view(['data' => $students, 'total' => count($students), 'per_page' => count($students), 'current_page' => 1, 'last_page' => 1], 200)
//            ->setTemplate("App:Student:index.html.twig")
//            ->setTemplateVar('data')
//            ->setTemplateData(['entities' => $entities])
//        ;
//
//        return $this->handleView($view);

        return ['pagination' => $pagination,
            'queryString' => $request->query->get('queryString', ''),
            'providerId' => $request->query->get('providerId', ''),
            'providers' => $providers, ];
    }

    /**
     * Finds and displays a Student entity.
     *
     * @Route("/student/{id}/show", name="student_show")
     * @Template()
     * @Secure(roles="ROLE_TEACHER")
     *
     * @param $id
     *
     * @return array
     */
    public function showAction(ConfigurationManager $configurationManager, $id)
    {
        if (!$configurationManager->getTeacherCanSeeList() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Student')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $ldapUser = $em->getRepository('App:LdapUser')->findOneByDistinguishedId($entity->getId());
        $entity->ldapUser = $ldapUser;
//        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
//            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to create a new Student entity.
     *
     * @Route("/student/new", name="student_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     *
     * @return array
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $student = new Student(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App:Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $student->setProvider($provider);
                $groups = $em->getRepository('App:Group')->findBy(['provider' => $provider, 'manageManually' => true], ['name' => 'ASC']);
            }
        }

        $groupId = $request->get('groupId', '');
        if ('' != $groupId) {
            $group = $em->getRepository('App:Group')->find($groupId);
            if ($group instanceof Group) {
                $student->addMemberOf($group);
            }
        }

        if (!isset($groups)) {
            $groups = $em->getRepository('App:Group')->findBy(['manageManually' => true]);
        }

        $form = $this->createForm('App\Form\StudentType', $student);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return [
            'student' => $student,
            'form' => $form->createView(),
            'groupId' => $groupId,
            'providerId' => $providerId,
            'groups' => $groups,
        ];
    }

    /**
     * Creates a new Student entity.
     *
     * @Route("/student/create", name="student_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("post")
     * @Template("Student/new.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $student = new Student(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App:Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $student->setProvider($provider);
                $groupsList = $em->getRepository('App:Group')->findBy(['provider' => $provider, 'manageManually' => true], ['name' => 'ASC']);
            }
        }

        $groupId = $request->get('groupId', '');
        if ('' != $groupId) {
            $group = $em->getRepository('App:Group')->find($groupId);
            if ($group instanceof Group) {
                $student->addMemberOf($group);
            }
        }

        $form = $this->createForm('App\Form\StudentType', $student);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($student->getProvider() instanceof Provider && $student->haveAdditionalProvider($student->getProvider())) {
                $student->removeAdditionalProvider($student->getProvider());
            }

            $em->persist($student);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');
            try {
                $repositoryAction->executeAfterCreate($student);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            if ('' != $groupId) {
                return $this->redirect($this->generateUrl('group_show', ['id' => $groupId]));
            } else {
                return $this->redirect($this->generateUrl('student_list'));
            }
        }

        if (!isset($groupsList)) {
            $groupsList = $em->getRepository('App:Group')->findBy(['manageManually' => true, 'provider' => $student->getProvider()->getId()]);
        }

        return [
            'entity' => $student,
            'form' => $form->createView(),
            'groupId' => $groupId,
            'providerId' => $providerId,
            'groups' => $groupsList,
        ];
    }

    /**
     * Displays a form to edit an existing Student entity.
     *
     * @Route("/student/{id}/edit", name="student_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('App:Student')->find($id);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $editForm = $this->createForm('App\Form\StudentType', $student, [
			'action' => $this->generateUrl('student_update', ['id' => $id, 'groupId' => $groupId]),
			'method' => 'POST'
		]);
        $editForm->add('submit', SubmitType::class, array('label' => 'Salva Modifiche'));
        $deleteForm = $this->createDeleteForm($id, $groupId);

        $providerId = '';
        if ($student->getProvider() instanceof Provider) {
            $providerId = $student->getProvider()->getId();
            $groups = $em->getRepository('App:Group')->findBy(['provider' => $student->getProvider(), 'manageManually' => true], ['name' => 'ASC']);
        } else {
            $groups = $em->getRepository('App:Group')->findBy(['manageManually' => true]);
        }

        return [
            'entity' => $student,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'groupId' => $groupId,
            'groups' => $groups,
            'providerId' => $providerId,
        ];
    }

    /**
     * Edits an existing Student entity.
     *
     * @Route("/student/{id}/update", name="student_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("post")
     * @Template("/student/edit.html.twig")
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('App:Student')->find($id);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $previousStudentEntity = clone $student;

        $editForm = $this->createForm('App\Form\StudentType', $student, [
			'action' => $this->generateUrl('student_update', ['id' => $id, 'groupId' => $groupId]),
			'method' => 'POST'
		]);
        $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $deleteForm = $this->createDeleteForm($id, $groupId);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($student->getProvider() instanceof Provider && $student->haveAdditionalProvider($student->getProvider())) {
                $student->removeAdditionalProvider($student->getProvider());
            }

            $em->persist($student);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');
            try {
                $repositoryAction->executeAfterUpdate($student, $previousStudentEntity);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $editForm->addError(new FormError($message));
                }
            }

            if ($editForm->getErrors()->count() < 1) {
                if ('' != $groupId) {
                    return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
                } else {
                    return $this->redirect($this->generateUrl('student_list'));
                }
            }
        }

        $providerId = '';
        if ($student->getProvider() instanceof Provider) {
            $providerId = $student->getProvider()->getId();
            $groups = $em->getRepository('App:Group')->findBy(['provider' => $student->getProvider(), 'manageManually' => true], ['name' => 'ASC']);
        } else {
            $groups = $em->getRepository('App:Group')->findBy(['manageManually' => true]);
        }

        return [
            'entity' => $student,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'groupId' => $groupId,
            'groups' => $groups,
            'providerId' => $providerId,
        ];
    }

    /**
     * Present the form to delete a Student entity.
     *
     * @Route("/student/{id}/remove", name="student_delete_form")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("get")
     * @Template("/student/delete.html.twig")
     */
    public function deleteFormAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('App:Student')->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $form = $this->createDeleteForm($id, $groupId);

        return ['form' => $form->createView(), 'entity' => $student, 'groupId' => $groupId];
    }

    /**
     * Compose delete form.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm($id, $groupId = '')
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('student_delete', ['id' => $id, 'groupId' => $groupId]))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', CheckboxType::class, ['label' => 'Cancellare da Active Directory', 'required' => false])
            ->add('deleteOnGApps', ChoiceType::class, ['expanded' => true, 'label' => 'Cancellare da Google Apps', 'choices' => ['Si' => 1, 'Spostare in Ou Indicata' => 2], 'required' => false, 'placeholder' => 'No'])
            ->add('gAppsOu', TextType::class, ['label' => 'OU Google Apps di destinazione (se specificato lo spostamento)', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Rimuovi', 'attr' => ['class' => 'btn btn-danger']])
            ->getForm();
    }

    /**
     * Deletes a Student entity.
     *
     * @Route("/student/{id}/delete", name="student_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("delete")
     */
    public function deleteAction($id, Request $request)
    {
        $groupId = $request->get('groupId', '');
        $form = $this->createDeleteForm($id, $groupId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $student = $em->getRepository('App:Student')->find($id);

            if (!$student) {
                throw $this->createNotFoundException('Unable to find Student entity.');
            }

            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');
            try {
                $repositoryAction->executeBeforeRemove($student, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData(), $form->get('gAppsOu')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($student);
            $em->flush();
        }

        if ('' != $groupId) {
            return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
        } else {
            return $this->redirect($this->generateUrl('student_list'));
        }
    }

    /**
     * Show the badge of a student.
     *
     * @Route("/student/{id}/badge", name="student_badge")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function printStudentBadgeAction(ConfigurationManager $configurationManager, Request $request, $id)
    {
        if (!$configurationManager->getTeacherCanSeeList() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('App:Student')->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }
        $ldapUser = $em->getRepository('App:LdapUser')->findOneByUsername($student->getUsername());
        $student->ldapUser = $ldapUser;

        return $this->printStudentBadge($student, $configurationManager, $request);
    }

    /**
     * Show current user badge.
     *
     * @Route("/student/printMyBadge", name="printMyBadge")
     */
    public function printMyBadgeAction(Request $request, ConfigurationManager $configurationManager)
    {
        $em = $this->getDoctrine()->getManager();
        $username = $this->get('security.token_storage')->getToken()->getUser()->getUsername();
        $student = $em->getRepository('App:Student')->findOneByUsername($username);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException("L'utenza corrente non sembra essere di uno studente");
        }
        $ldapUser = $em->getRepository('App:LdapUser')->findOneByDistinguishedId($student->getId());
        $student->ldapUser = $ldapUser;

        return $this->printStudentBadge($student, $configurationManager, $request);
    }

    /**
     * Print the student badge in html or pdf format.
     *
     * @return Response
     *
     * @throws \Exception
     */
    protected function printStudentBadge(Student $student, ConfigurationManager $configurationManager, Request $request)
    {
        $format = $request->get('_format', 'pdf');
        $configurationAD = $configurationManager->getActiveDirectoryConfiguration();

        $pdfHeader = $pdfFooter = $pdfBody = '';
        $provider = $student->getProvider();
        if ($provider instanceof Provider) {
            if ('' != $provider->getPdfHeader()) {
                $pdfHeader = $provider->getPdfHeader();
            }

            if ('' != $provider->getPdfFooter()) {
                $pdfFooter = $provider->getPdfFooter();
            }

            $studentGroups = [];
            foreach ($student->getMemberOf() as $group) {
                $studentGroups[] = $group->getName();
            }

            if ('' != $provider->getPdfStudentBadge()) {
                $pdfBody = $provider->getPdfStudentBadge();
                $pdfBody = str_replace([
                    '{studentFirstName}',
                    '{studentLastName}',
                    '{studentUsername}',
                    '{studentEmail}',
                    '{studentStartingPassword}',
                    '{studentFiscalCode}',
                    '{studentGroups}',
                    '{studentHomeFolder}',
                ], [
                    $student->getFirstname(),
                    $student->getLastname(),
                    $student->getUsername(),
                    $student->ldapUser->getEmail(),
                    $student->getStartingPassword(),
                    $student->getFiscalCode(),
                    implode(' ', $studentGroups),
                    '', //$configurationAD
                ], $pdfBody);
            }
        }

        $out = $this->render(sprintf('student/badge.%s.twig', $format), [
            'student' => $student,
            'pdfHeader' => $pdfHeader,
            'pdfFooter' => $pdfFooter,
            'pdfBody' => $pdfBody,
            'headerHeight' => $provider->getPdfHeaderHeight() > 0 ? $provider->getPdfHeaderHeight() : ('' != $pdfHeader ? 20 : 0),
            'footerHeight' => $provider->getPdfFooterHeight() > 0 ? $provider->getPdfFooterHeight() : ('' != $pdfFooter ? 20 : 0),
            'configurationAD' => $configurationAD,
        ]);

        if ('pdf' == $format) {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($out->getContent());
            $dompdf->getOptions()->set('isRemoteEnabled', true);
            if ('' != $provider->getPdfStudentBadgePageSize()) {
                $dompdf->setPaper($provider->getPdfStudentBadgePageSize(), $provider->getPdfStudentBadgePageLandscape() ? 'landscape' : 'portrait');
            }

            $dompdf->render();
            $out->setContent($dompdf->output());
            $out->headers->set('Content-Type', 'application/pdf');
        }

        return $out;
    }

    /**
     * Ajax call to reset a student password.
     *
     * @Route("/student/reset_password_ajax", name="student_reset_password_ajax")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function resetPasswordAjaxAction(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, Request $request)
    {
        $studentId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($configurationManager, $personsAndGroups, $studentId, $targetService, false);
    }

    /**
     * Ajax call to renew a student password.
     *
     * @Route("/student/renew_password_ajax", name="student_renew_password_ajax")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function renewPasswordAjaxAction(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, Request $request)
    {
        $studentId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($configurationManager, $personsAndGroups, $studentId, $targetService, true);
    }

    /**
     * Reset or renew a student password, anserwing in json.
     *
     * @param $studentId
     * @param $targetService
     * @param bool $renew
     *
     * @return Response
     */
    protected function resetPassword(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, $studentId, $targetService, $renew = false)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();

        if (!$configurationManager->getTeacherCanResetPassword() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Accesso negato']));

            return $response;
        }

        $student = $em->getRepository('App:Student')->find($studentId);
        if (!$student instanceof Student) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Utente specificato non valido']));

            return $response;
        }

        if (0 == strlen(trim($student->getStartingPassword())) && !$renew) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Password di default non generata']));

            return $response;
        }

        $newPassword = '';
        if ($renew) {
            $newPassword = $personsAndGroups->getRandomPassword($configurationManager->getActiveDirectoryPasswordComplexity(), $configurationManager->getActiveDirectoryPasswordMinChar());
            if (0 == strlen(trim($newPassword))) {
                $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante la generazione della password']));

                return $response;
            }

            $student->setStartingPassword($newPassword);
        }
        if ('ad' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($student, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante il tentativo di cambio password di rete']));

                return $response;
            }
        }

        if ('email' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($student, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => "Errore durante il tentativo di cambio password dell'account Google"]));

                return $response;
            }
        }

        $response->setContent(json_encode(['success' => true, 'newPassword' => $newPassword]));

        return $response;
    }

    /**
     * Send the student credential to the specified email.
     *
     * @Route("/student/{id}/send_credential_to_email", name="student_send_credential_to_email")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function sendCredentialToEmailAction(ConfigurationManager $configurationManager, Request $request, $id)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $em = $this->getDoctrine()->getManager();

        if (!$configurationManager->getTeacherCanSeeList() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Accesso negato']));

            return $response;
        }

        $student = $em->getRepository('App:Student')->find($id);
        if (!$student instanceof Student) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Studente non valido']));

            return $response;
        }

        $email = trim($request->get('emailTo', ''));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Email specificata non valida']));

            return $response;
        }

        $body = <<<MAIL
Riepilogo credenziali per lo studente {$student->getLastname()} {$student->getFirstname()}:

Username: {$student->getUsername()}
E-Mail: {$student->getEmail()}
Password: {$student->getStartingPassword()}
MAIL;

        $message = new \Swift_Message("Credenziali per lo studente {$student->getLastname()} {$student->getFirstname()}", $body);
        $message->setTo($email)->setFrom('no-reply@igroove.network');
        if ($this->get('mailer')->send($message)) {
            $response->setContent(json_encode(['success' => true]));
        } else {
            $response->setContent(json_encode(['success' => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }

//    /**
//     * @GET("/students.{_format}", name="api_students", defaults={"_format" = "html"})
//     */
//    public function getStudentsAction(Request $request) {
////        $perPage = (int)$request->get("per_page", 20);
////        $page = (int)$request->get('page', 1);
////        $limit = $page > 0 ? ($page - 1) * $perPage : 0;
//        $cleanedStudents = [];
//        $studentCount = 0;
//
//
//        $em = $this->getDoctrine()->getManager();
////        $students = $em->getRepository('App:Student')->findBy([], null, $perPage, $limit);
////        $qb = $em->getRepository('App:Student')->createQueryBuilder("students");
////        $studentCount = (int)$qb->select($qb->expr()->count('students.id'))->getQuery()->getSingleScalarResult();
////        $last = $studentCount > 0 ? ceil($studentCount/$perPage) : 0;
//
//        $distinct = true;
////        $query = $em->getRepository('App:Student')->createQueryBuilder('student');
////
////        $sort = $request->get('sort', '');
////        if($sort !== "") {
////            foreach (explode(",", $sort) as $sortItem) {
////                $sortItemExploded = explode("|", $sortItem);
////                if(empty($sortItemExploded)) {
////                    continue;
////                }
////                if(substr($sortItemExploded[0], 0, 9) == "provider.") {
////                    $query->join('student.provider', 'provider');
////                }
////                if(substr($sortItemExploded[0], 0, 6) == "group.") {
////                    $query->join('student.memberOf', 'group');
////                    $distinct = false;
////                }
////                $query->addOrderBy($sortItemExploded[0], isset($sortItemExploded[1]) ? $sortItemExploded[1] : "asc");
////            }
////
////            if(isset($_GET['sort'])) {
////                unset($_GET['sort']);
////            }
////        }
////
////        foreach (['firstname', 'lastname', 'username', 'email'] as $fieldName) {
////            if($request->query->has($fieldName) && $request->query->get($fieldName, '') !== "") {
////                $query->andWhere($query->expr()->like('student.'.$fieldName, $query->expr()->literal("%".$request->query->get($fieldName)."%")));
////            }
////        }
//
//        $query = $this->buildQueryFromFilterInput($request);
//
//        $paginator  = $this->get('knp_paginator');
//        $pagination = $paginator->paginate(
//            $query,
//            (int)$request->query->get('page', 1),
//            (int)$request->get("per_page", 20),
//            ['distinct' => $distinct]
//        );
//
////        foreach ($pagination as $student) {
////            $groupsName = [];
////            foreach ($student->getGroups() as $group) {
////                $groupsName[] = $group->getName();
////            }
////            $cleanedStudents[] = [
////                'id' => $student->getId(),
////                'fiscalCode' => $student->getFiscalCode(),
////                'firstname' => $student->getFirstname(),
////                'lastname' => $student->getLastname(),
////                'groups' => implode(", ", $groupsName),
////                'username' => $student->getUsername(),
////                'startingPassword' => $student->getStartingPassword(),
////                'email' => $student->getEmail(),
////                'providerName' => $student->getProvider()->getName(),
////            ];
////        }
//
////        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
////        $normalizer = new ObjectNormalizer($classMetadataFactory);
////        $normalizer->setCircularReferenceLimit(1);
////        $serializer = new Serializer(array($normalizer));
////        $data = $serializer->normalize($pagination->getItems(), null);
//
//
////        $context = SerializationContext::create()->enableMaxDepthChecks()->setGroups(['Default', 'student_groups', 'person_provider']);
////        $serializer = \JMS\Serializer\SerializerBuilder::create()
////            ->build();
////        $data = $serializer->toArray($pagination->getItems(), $context);
////
////
////        var_dump($data);
////        die();
////        var_dump($pagination->getItems());
//
//        $context2 = new \FOS\RestBundle\Context\Context();
//        $context2->enableMaxDepth()->setGroups(['Default', 'student_groups', 'person_provider', 'group_sector']);
//
////        $view = $this->view(['data' => $cleanedStudents, 'pagination' => ['total' => $studentCount, 'per_page' => $perPage, 'current_page' => $page, 'from' => $limit, 'to' => $limit + $perPage, 'last_page' => $last]], 200)
//        $view = $this->view(['data' => $pagination->getItems(), 'pagination' => ['total' => $pagination->getTotalItemCount(), 'per_page' => $pagination->getItemNumberPerPage(), 'current_page' => $pagination->getCurrentPageNumber(), 'last_page' => ($pagination->getTotalItemCount() > 0 ? ceil($pagination->getTotalItemCount()/$pagination->getItemNumberPerPage()):0)]], 200)
//            ->setTemplate("App:Student:index.html.twig")
//            ->setTemplateVar('data')
//            ->setContext($context2)
////            ->setTemplateData(['entities' => $students])
//        ;
//
//        return $this->handleView($view);
//    }
//
//    public function buildQueryFromFilterInput(Request $request) {
//        $filtersData = [
//            ['name' => 'firstname'],
//            ['name' => 'lastname'],
//            ['name' => 'username'],
//            ['name' => 'email'],
//            ['name' => 'memberOf'],
//        ];
//
//        $entityClass = Student::class;
//        $em = $this->getDoctrine()->getManager();
//        $query = $em->getRepository('App:Student')->createQueryBuilder('student');
//        $proxyQuery = new ProxyQuery($query);
//
////        $data = $request->query->all();
//
//        $filters = $this->buildFilters($filtersData, $entityClass);
//
//        foreach ($filters as $name => $filter) {
//            if(!$request->query->has($filter->getFormName())) {
//                continue;
//            }
//
//            $data = $request->query->get($filter->getFormName());
//            if(is_string($data)) {
//                $data = ['value' => $data];
//            }
//
////            $this->values[$name] = isset($this->values[$name]) ? $this->values[$name] : null;
//            $filter->apply($proxyQuery, $data);
//        }
//
//        return $proxyQuery->getQueryBuilder();
//    }
//
//    /**
//     * @param $filtersData
//     * @param $entityClass
//     * @return \Sonata\AdminBundle\Filter\FilterInterface[]|array
//     */
//    public function buildFilters($filtersData, $entityClass) {
////        /** @var ModelManager $modelManager */
//        $modelManager = $this->get('sonata.admin.manager.orm');
////        /** @var \Sonata\DoctrineORMAdminBundle\Guesser\FilterTypeGuesser $guesser */
//        $guesser = $this->get('sonata.admin.guesser.orm_datagrid');
//        $filterFactory = $this->get('sonata.admin.builder.filter.factory');
//
//        $buildedFilters = [];
//        foreach ($filtersData as $filterData) {
//            if(is_string($filterData)) {
//                $filterData = ['name' => $filterData];
//            } elseif(!is_array($filterData) || !isset($filterData['name'])) {
//                continue;
//            }
//
//            if(!isset($filterData['options']) || !is_array($filterData['options'])) {
//                $filterData['options'] = [];
//            }
//
//            if(isset($filterData['field_options']) && is_array($filterData['field_options'])) {
//                $filterData['options']['field_options'] = $filterData['field_options'];
//            }
//
//            if(isset($filterData['field_type']) && $filterData['field_type']) {
//                $filterData['options']['field_type'] = $filterData['field_type'];
//            }
//
//            $filterData['options']['field_name'] = isset($filterData['options']['field_name']) ? $filterData['options']['field_name'] : substr(strrchr('.'.$filterData['name'], '.'), 1);
//
//            $fieldDescription = $modelManager->getNewFieldDescriptionInstance(
//                $entityClass,
//                $filterData['name'],
//                $filterData['options']
//            );
//
//            if(!isset($filterData['type'])) {
//                $guessType = $guesser->guessType($entityClass, $filterData['name'], $modelManager);
//                $filterData['type'] = $guessType->getType();
//
//                $options = $guessType->getOptions();
//
//                foreach ($options as $name => $value) {
//                    if (is_array($value)) {
//                        $fieldDescription->setOption($name, array_merge($value, $fieldDescription->getOption($name, array())));
//                    } else {
//                        $fieldDescription->setOption($name, $fieldDescription->getOption($name, $value));
//                    }
//                }
//            }
//
//            $fieldDescription->setType($filterData['type']);
//
//            if ($modelManager->hasMetadata($entityClass)) {
//                list($metadata, $lastPropertyName, $parentAssociationMappings) = $modelManager->getParentMetadataForProperty($entityClass, $fieldDescription->getName());
//
//                // set the default field mapping
//                if (isset($metadata->fieldMappings[$lastPropertyName])) {
//                    $fieldDescription->setOption('field_mapping', $fieldDescription->getOption('field_mapping', $metadata->fieldMappings[$lastPropertyName]));
//
//                    if ($metadata->fieldMappings[$lastPropertyName]['type'] == 'string') {
//                        $fieldDescription->setOption('global_search', $fieldDescription->getOption('global_search', true)); // always search on string field only
//                    }
//                }
//
//                // set the default association mapping
//                if (isset($metadata->associationMappings[$lastPropertyName])) {
//                    $fieldDescription->setOption('association_mapping', $fieldDescription->getOption('association_mapping', $metadata->associationMappings[$lastPropertyName]));
//                }
//
//                $fieldDescription->setOption('parent_association_mappings', $fieldDescription->getOption('parent_association_mappings', $parentAssociationMappings));
//            }
//
//            $fieldDescription->setOption('code', $fieldDescription->getOption('code', $fieldDescription->getName()));
//            $fieldDescription->setOption('name', $fieldDescription->getOption('name', $fieldDescription->getName()));
//
//            $fieldDescription->mergeOption('field_options', array('required' => false));
//            $filter = $filterFactory->create($fieldDescription->getName(), $filterData['type'], $fieldDescription->getOptions());
//
////            if (false !== $filter->getLabel() && !$filter->getLabel()) {
////                $filter->setLabel($admin->getLabelTranslatorStrategy()->getLabel($fieldDescription->getName(), 'filter', 'label'));
////            }
//
//            $buildedFilters[] = $filter;
//        }
//
//        return $buildedFilters;
//    }
}
