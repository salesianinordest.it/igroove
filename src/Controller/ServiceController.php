<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Supervisor\Connector\XmlRpc;
use Supervisor\Supervisor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use JMS\SecurityExtraBundle\Annotation\Secure;


class ServiceController extends BaseAbstractController
{
    /**
     * @Route("/service", name="service")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {

        $configurationManager = $this->get('zen.igroove.configuration');

        $servicesList = array();
        if ($configurationManager->getWirelessUnifiController()) {
            $servicesList['unifi'] = 'Server Ubiquiti Unifi';
        }
        $em = $this->getDoctrine()->getManager();
        $mikrotiks = $em->getRepository('App:Mikrotik')->findAll();
        if (sizeof($mikrotiks)) {
            $servicesList['mikrotik'] = 'Firewall Mikrotik';
        }

        $servicesList['radius'] = 'Server RADIUS';

        $supervisor = $this->getSupervisorStatus();

        return array(
            'servicesList' => $servicesList,
            'supervisor' => $supervisor
        );
    }

    /**
     * @Route("/service-restart/{service}", name="service_restart")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function restartAction($service)
    {
        $serviceRestarter = $this->get('zen.igroove.serviceRestarter');

        $messages = $serviceRestarter->restart($service);

        return array(
            'messages' => $messages
        );
    }

    protected function getSupervisorStatus()
    {
        $supervisorStatus = ['working' => false, 'processes' => []];

        try {
            $guzzleClient = new Client(['auth' => ['iGroove', 'iGroovePwd']]);
            $client = new \fXmlRpc\Client('http://127.0.0.1:9001/RPC2',
                new \fXmlRpc\Transport\HttpAdapterTransport(new \Http\Message\MessageFactory\DiactorosMessageFactory(), new \Http\Adapter\Guzzle6\Client($guzzleClient))
            );

            $connector = new XmlRpc($client);
            $supervisor = new Supervisor($connector);

            $state = $supervisor->getState();
            if(is_array($state) && isset($state['statecode']) && $state['statecode'] === 1) {
                $supervisorStatus['working'] = true;
            }

            foreach ($supervisor->getAllProcesses() as $process) {
                $payload = $process->getPayload();

                $supervisorStatus['processes'][] = [
                    'name' => $payload['name'],
                    'status' => $payload['statename']." (".$payload['description'].")"
                ];
            }
        } catch (\Exception $e) {}

        return $supervisorStatus;
    }
}
