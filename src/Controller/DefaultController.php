<?php

namespace App\Controller;

use App\Manager\ConfigurationManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\VersionManager;

class DefaultController extends BaseAbstractController
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(VersionManager $versionManager, ConfigurationManager $configurationManager)
    {
        $versions = $versionManager->getVersions();


        return array(
            'version' => $versions,
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent()),
        );
    }

    /**
     * @Route("/menu", name="menu")
     * @Template()
     */
    public function menuAction(Request $request, ConfigurationManager $configurationManager)
    {
        $activeMenu = ['homepage' => '', 'guest' => '', 'user' => '', 'teacher' => '', 'administrator' => '', 'network' => ''];

        switch ($request->get('_route')) {
            case 'homepage':
                $activeMenu['homepage'] = 'active';
                break;
            case 'guest':
            case 'guest_new':
            case 'guest_edit':
                $activeMenu['guest'] = 'active';
                break;
            case 'changeMyPassword':
                $activeMenu['user'] = 'changeMyPassword';
                break;
            case 'config_edit':
                $activeMenu['administrator'] = 'config';
                break;
            case 'mikrotik':
            case 'config_mikrotik_new':
            case 'config_mikrotik_create':
            case 'config_mikrotik_edit':
                $activeMenu['network'] = 'mikrotik';
                break;
        }

        return array(
            'activeMenu' => $activeMenu,
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent()),
        );
    }

    /**
     * @Route("/menu-top", name="menuTop")
     * @Template()
     */
    public function menuTopAction(Request $request, ConfigurationManager $configurationManager)
    {
        return array(
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent()),
        );
    }

    /**
     * @Route("/footer", name="footer")
     * @Template()
     */
    public function footerAction(VersionManager $versionManager)
    {
        return array(
            'version' => $versionManager->getVersions(),
        );
    }

    /**
     * @Route("/credits", name="credits")
     * @Template()
     */
    public function creditsAction()
    {
        return array();
    }

    /**
     * @Route("/help", name="help")
     * @Template()
     */
    public function helpAction(ConfigurationManager $configurationManager)
    {
        return array(
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent()),
        );
    }

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction(VersionManager $versioneManager, Request $request, LoggerInterface $logger)
    {
        $error = $this->getAuthenticationError($request);

        $versions = $versioneManager->getVersions();

        $randomImages = [];
        $projectDir = $this->getParameter('kernel.project_dir');
        $imagesPath = $projectDir . '/public/images/login';
        if (file_exists($imagesPath)) {
            $d = dir($imagesPath);
            while (false !== ($entry = $d->read())) {
                if ('.jpg' == substr($entry, -4)) {
                    $randomImages[] = $entry;
                }
            }

            $d->close();
        }
        if (sizeof($randomImages) > 0) {
            $randomImage = $randomImages[array_rand($randomImages)];
        } else {
            $randomImage = '';
        }

        return
            array(
                'last_username' => $request->getSession()->get(Security::LAST_USERNAME),
                'error' => $error,
                'token' => $this->generateToken(),
                'version' => $versions,
                'randomImage' => $randomImage,
            );
    }

    protected function getAuthenticationError(Request $request)
    {
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            return $request->attributes->get(Security::AUTHENTICATION_ERROR);
        }

        return $request->getSession()->get(Security::AUTHENTICATION_ERROR);
    }

    protected function generateToken()
    {
        $token = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();

        return $token;
    }

    protected function currentUserIsStudent()
    {
        static $isStudent;

        if (!isset($isStudent)) {
            $em = $this->getDoctrine()->getManager();
            $isStudent = false;
            if ($this->get('security.token_storage')->getToken() instanceof TokenInterface) {
                $username = $this->get('security.token_storage')->getToken()->getUsername();
                $isStudent = $em->getRepository('App:Student')->userIsStudent($username);
            }
        }

        return $isStudent;
    }
}
