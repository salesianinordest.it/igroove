<?php

namespace App\Controller;

use App\Entity\AppleSchool;
use App\Entity\AppleSchoolLocation;
use App\Form\AppleSchoolType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

/**
 * Apple School controller.
 *
 * @Route("/config/appleSchool")
 */
class AppleSchoolController extends BaseAbstractController {

	/**
	 * Creates a new Provider entity.
	 *
	 * @Route("/", name="config_apple_school_create")
	 * @Secure(roles="ROLE_ADMIN")
	 * @Template("apple_school/edit.html.twig")
	 */
	public function createAction(Request $request)
	{
		$appleSchool = new AppleSchool();
		$form = $this->createForm(AppleSchoolType::class, $appleSchool, array(
			'action' => $this->generateUrl('config_apple_school_create'),
			'method' => 'PUT',
		));
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($appleSchool);
			$em->flush();

			return $this->redirect($this->generateUrl('config_edit') . '#appleSchool');
		}

		return array(
			'entity' => $appleSchool,
			'form' => $form->createView(),
		);
	}

	/**
	 * Displays a form to edit an existing Provider entity.
	 *
	 * @Route("/{id}/edit", name="config_apple_school_edit")
	 * @Secure(roles="ROLE_ADMIN")
	 * @Template()
	 */
	public function editAction(Request $request, AppleSchool $appleSchool)
	{
		if (!$appleSchool) {
			throw $this->createNotFoundException('Unable to find Provider entity.');
		}

		/** @var AppleSchoolLocation[]|ArrayCollection $originalLocations */
		$originalLocations = new ArrayCollection();
		foreach ($appleSchool->getLocations() as $location) {
			$originalLocations->add($location);
		}

		$editForm = $this->createForm(AppleSchoolType::class, $appleSchool, [
			'action' => $this->generateUrl('config_apple_school_edit', ['id' => $appleSchool->getId()]),
			'method' => 'PUT'
		]);
		$editForm->handleRequest($request);

		if ($editForm->isSubmitted() && $editForm->isValid()) {
			foreach ($originalLocations as $location) {
				if (FALSE === $appleSchool->getLocations()->contains($location)) {
					foreach($location->getProviders() as $provider) {
						$provider->setAppleSchoolLocation(null);
					}
//					$location->getLocations()->removeElement($appleSchool);
					$this->getDoctrine()->getManager()->remove($location);
				}
			}
			$this->getDoctrine()->getManager()->flush();
			return $this->redirect($this->generateUrl('config_edit', []) . '#appleSchool');
		}

		return array(
			'entity' => $appleSchool,
			'form' => $editForm->createView(),
		);
	}

	/**
	 * Deletes a Provider entity.
	 *
	 * @Route("/{id}", name="config_apple_school_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_ADMIN")
	 */
	public function deleteAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App:AppleSchool')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Apple School entity.');
		}

		$em->remove($entity);
		$em->flush();

		return $this->redirect($this->generateUrl('config_edit') . '#appleSchool');
	}

}