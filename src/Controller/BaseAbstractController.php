<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

abstract class BaseAbstractController extends Controller {

    protected $container;

    protected $logger;
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container,LoggerInterface $logger)
    {
        $this->container = $container;
        $this->logger=$logger;
    }

    protected function getCurrentUserUsername() {
        static $currentUserName;

        if(!isset($currentUserName)) {
            $currentUserName = "";
            if($this->get('security.token_storage')->getToken() instanceof TokenInterface) {
                $currentUserName = $this->get('security.token_storage')->getToken()->getUsername();
            }
        }

        return $currentUserName;
    }

    protected function logAction($action, $info) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->logger->info("[{$action}] ".$this->getCurrentUserUsername()."({$ip}) {$info}");
    }
}