<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use App\Manager\ConfigurationManager;

/**
 * Configuration controller.
 */
class ConfigurationController extends BaseAbstractController
{
    /**
     * List Configuration.
     *
     * @Route("/config/edit", name="config_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request, ConfigurationManager $configurationManager)
    {
        $em = $this->getDoctrine()->getManager();

        $appleSchools = $em->getRepository('App:AppleSchool')->findAll();
        $providers = $em->getRepository('App:Provider')->findAll();

        $form = $this->createForm('App\Form\ConfigurationType', $configurationManager, array(
            'action' => $this->generateUrl('config_update'),
            'method' => 'POST',
        ));

        return array(
            'form' => $form->createView(),
            'saved' => $request->get('saved', false),
            'appleSchools' => $appleSchools,
            'providers' => $providers,
        );
    }

    /**
     * Edit a Configuration.
     *
     * @Route("/config/update", name="config_update")
     * @Method("post")
     * @Template("configuration/edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, ConfigurationManager $configurationManager)
    {
        $form = $this->createForm('App\Form\ConfigurationType', $configurationManager);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $configurationManager->save();

            return $this->redirect($this->generateUrl('config_edit', array('saved' => true)));
        }

        return array(
            'form' => $form->createView(),
            'saved' => false,
        );
    }
}
