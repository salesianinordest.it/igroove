<?php

namespace App\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\InternetOpen;
use App\Entity\MikrotikList;
use App\Form\MikrotikListType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * MikrotikList controller.
 *
 * @Route("/mikrotik/list")
 */
class MikrotikListController extends BaseAbstractController
{


    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik_list")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("mikrotik_list/index.html.twig")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request)
    {
        $queryString = $request->get('queryString', false);
        $q = '%' . $queryString . '%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                'SELECT l FROM App:MikrotikList l WHERE l.nome  LIKE :q ORDER BY l.nome'
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery('SELECT l FROM App:MikrotikList l  ORDER BY l.nome');
        }

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserName = "";
        $currentUserIsAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        if($currentUser instanceof \IMAG\LdapBundle\User\LdapUser){
            $currentUserName = $currentUser->getUsername();
        }

        $mikrotikLists = $query->getResult();
        foreach ($mikrotikLists as $k => $mikrotikList) {
            $mikrotikLists[$k]->state = "not-active";
            $mikrotikLists[$k]->endingTime = "";
            $mikrotikLists[$k]->activedByOtherUser = false;
            $mikrotikLists[$k]->activedBy = "";

            $internetOpen = $em->getRepository('App:InternetOpen')->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
            if($internetOpen instanceof InternetOpen) {
                if(!$internetOpen->getActivationCompleated()) {
                    $mikrotikLists[$k]->state = "activating";
                } else {
                    $mikrotikLists[$k]->state = "active";
                    $mikrotikLists[$k]->endingTime = $internetOpen->getCloseAt()->format("H:i");

                    $mikrotikLists[$k]->activedBy = $currentUserIsAdmin && $internetOpen->getActivedBy() != "" ? $internetOpen->getActivedBy() : "";
                    if($currentUserName != "" && $internetOpen->getActivedBy() != $currentUserName) {
                        $mikrotikLists[$k]->activedByOtherUser = true;
                    }
                }
            }
        }

        $pagination = $paginator->paginate(
            $mikrotikLists,
            $request->query->get('page', 1),
            25
        );

        return array(
            'pagination' => $pagination,
            'timeToSet' => new \DateTime("+1 hour")
        );
    }

    /**
     * Creates a new MikrotikList entity.
     *
     * @Route("/create", name="mikrotik_list_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("mikrotik_list/new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }



    /**
     * Displays a form to create a new MikrotikList entity.
     *
     * @Route("/new", name="mikrotik_list_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template("mikrotik_list/new.html.twig")
     */
    public function newAction()
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    protected function createCreateForm($entity) {
    	return $this->createForm('App\Form\MikrotikListType', $entity, [
			'action' => $this->generateUrl('mikrotik_list_create'),
			'method' => 'POST',
		]);
	}

    /**
     * Displays a form to edit an existing MikrotikList entity.
     *
     * @Route("/{id}/edit", name="mikrotik_list_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template("mikrotik_list/edit.html.twig")
     */
    public function editAction(Request $request,MikrotikList $mikrotikList)
    {
        if (!$mikrotikList) {
            throw $this->createNotFoundException('Unable to find Mikrotik  list entity.');
        }

		$editForm = $this->createEditForm($mikrotikList);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('mikrotik_list', array()));
        }

        return array(
            'entity' => $mikrotikList,
            'edit_form' => $editForm->createView()
        );
    }



    /**
     * Edits an existing MikrotikList entity.
     *
     * @Route("/{id}/update", name="mikrotik_list_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("PUT")
     * @Template("mikrotik_list/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:MikrotikList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

	protected function createEditForm($entity) {
		return $this->createForm('App\Form\MikrotikListType', $entity, [
			'action' => $this->generateUrl('mikrotik_list_update', ['id' => $entity->getId()]),
			'method' => 'POST',
		]);
	}

    /**
     * Deletes a MikrotikList entity.
     *
     * @Route("/{id}/delete", name="mikrotik_list_delete")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App:MikrotikList')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find MikrotikList entity.');
		}

		$em->remove($entity);
		$em->flush();

        return $this->redirect($this->generateUrl('mikrotik_list'));
    }

}
