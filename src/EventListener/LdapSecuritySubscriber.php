<?php

namespace App\EventListener;

use App\Security\LdapUserProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
//use \IMAG\LdapBundle\Event\LdapUserEvent;
use App\Exception\NeedPasswordChangeAuthenticationException;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class LdapSecuritySubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents()
    {
        return array(
//         AuthenticationEvents::AUTHENTICATION_FAILURE=> 'onPreBind',
        );
    }

    /**
     * Modifies the User before binding data from LDAP
     *
     * @throws \Exception
     */

    public function onPreBind(BadCredentialsException $event)
    {

var_dump($event->getAuthenticationToken());
        $user = $event->getUser();

//        if((int)$user->getAttribute('useraccountcontrol') < 1 || (int)$user->getAttribute('useraccountcontrol') & 0x0002) {
//            throw new LockedException(sprintf('LDAP user %s is disabled', $user->getUsername()));
//        }

        if($user->getAttribute('pwdlastset') != "" && (int)$user->getAttribute('pwdlastset') < 1) {
            throw new NeedPasswordChangeAuthenticationException($user, "The password for the user {$user->getUsername()} is expired.");
        }
    }

}
