<?php

namespace App\Consumer;


use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Manager\MikrotikManager;

class MikrotikConsumer extends AbstractConsumer
{

    /**
     * @var MikrotikManager
     */
    private $mikrotikManager;

    public function __construct(MikrotikManager $mikrotikManager)
    {
        $this->mikrotikManager = $mikrotikManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = unserialize($msg->body);
        if (!array_key_exists('command', $message)) {
            return ConsumerInterface::MSG_ACK;
        }

        try {
            switch ($message['command']) {
                case 'addMacToHospot':
                    $results = $this->mikrotikManager->manageMacToHotspot();
                    break;

                case 'addIpReservation':
                    $results = $this->mikrotikManager->addIpReservation();
                    break;

                case 'addIpList':
                    $results = $this->mikrotikManager->addIpToIpList();
                    break;

                case 'addIpToBypassedList':
                    $results = $this->mikrotikManager->addIpToBypassedList();
                    break;

                case 'addPools':
                    $results = $this->mikrotikManager->addPools();
                    break;
            }
        } catch(\Throwable $e) {
            return $this->retryOnError($msg, $e);
        }

        if (isset($results) && !empty($results)) {
            print_r($results);
            //@todo riscrivere con il logger
        }

        return ConsumerInterface::MSG_ACK;
    }
}