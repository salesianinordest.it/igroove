<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use App\Exception\ConnectionException;

abstract class AbstractConsumer implements ConsumerInterface
{

    const ExceptionRetry = [30];
    const ConnectionExceptionRetry = [30, 90, 300];

    public function retryOnError(AMQPMessage $msg, $exception = null)
    {
        $retry = self::ExceptionRetry;
        if ($exception instanceof ConnectionException) {
            $retry = self::ConnectionExceptionRetry;
        }

        if (!$msg->has('application_headers') || !$msg->get('application_headers') instanceof AMQPTable) {
            $msg->set('application_headers', new AMQPTable());
        }
        $applicationHeaders = $msg->get('application_headers');
        $retryCount = isset($applicationHeaders->getNativeData()['x-death']) && isset($applicationHeaders->getNativeData()['x-death'][0]) && isset($applicationHeaders->getNativeData()['x-death'][0]['count']) && $applicationHeaders->getNativeData()['x-death'][0]['count'] > 0 ? $applicationHeaders->getNativeData()['x-death'][0]['count'] : 0;

        if (isset($retry[$retryCount])) {
            $msg->set('expiration', $retry[$retryCount] * 1000);
            $exchange = substr($msg->delivery_info['exchange'], -6) == "_retry" ? $msg->delivery_info['exchange'] : $msg->delivery_info['exchange'] . "_retry";
            $msg->delivery_info['channel']->basic_publish($msg, $exchange);
        }

        return ConsumerInterface::MSG_REJECT;
    }

}