<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LdapUser extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('distinguishedId')
            ->add('username')
            ->add('operation')->add('dn')->add('firstname')->add('lastname')->add('attributes')->add(
                'starting_password'
            );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper ->add('distinguishedId')->add('username')
            ->add('operation')->add('dn')->add('firstname')->add('lastname')->add('attributes');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('distinguishedId')
            ->add('username')
            ->add('operation')->add('dn')->add('firstname')->add('lastname')->add('')->add('attributes');
    }

}
