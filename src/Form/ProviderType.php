<?php

namespace App\Form;

use App\Entity\AppleSchoolLocation;
use Dompdf\Adapter\CPDF;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Manager\ConfigurationManager;

class ProviderType extends AbstractType
{
    protected $filterProviderData;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->setFilterProviderData($options['filterProviderData']);
        $builder
            ->add('name', TextType::class, array('label' => 'Nome del provider:', 'required' => false))
            ->add('filter',ChoiceType::class,
                array(
                    'choices' => $this->generateFilterSelect(),
                    'label' => 'Processare i dati secondo lo standard:',
                    'required' => false
                )
            )
			->add('appleSchoolLocation', EntityType::class, ['class' => AppleSchoolLocation::class, 'choice_label' => "full_name", 'label' => "Sede AppleSchool (Lasciare vuota per non usare)", 'required' => false])
            ->add('active', CheckboxType::class, array('label' => 'E\' attivo questo provider', 'required' => false));

        $this->generateFilterForm($builder);

        $builder
            ->add('studentAutoCreateUsername', CheckboxType::class,
                array(
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false
                )
            )
			->add('studentLdapAutoUsernameStyle', ChoiceType::class,
				array('label' => 'Sintassi dell\'username: (se diversa da quella generale)', 'required' => false,
					  'choices' => ConfigurationManager::$usernameStyles
				))
            ->add('studentForceImportedPasswordInitial', CheckboxType::class,
                array('label' => "Imposta la password proveniente dal provider alla creazione dell'utente?", 'required' => false)
            )
            ->add('studentForceImportedPassword', CheckboxType::class,
                array('label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false)
            )
			->add('studentLdapExtendedUsernameSuffix', TextType::class,
				['label' => "Suffisso terminale/dominio per l'username esteso Active Directory (lasciare vuoto per usare il nome dominio generale)", 'required' => false]
			)
			->add('studentLdapUseEmailForExtendedUsername', CheckboxType::class,
				array('label' => "Usa la prima parte dell'email come username esteso di Active Directory?", 'required' => false)
			)
			->add('studentsOrderByInTeacherList', ChoiceType::class,
				['label' => "Ordina Studenti nella lista per i docenti per", 'choices' => ['Username' => 'username', 'Cognome' => 'lastname', 'Nome' => 'firstname']])
			->add('studentLdapCreateOU', CheckboxType::class,
                array('label' => 'Crea le unità organizzative', 'required' => false)
            )
            ->add('studentLdapOUPrefix', TextType::class,
                array('label' => 'Prefisso al nome delle unità organizzative', 'required' => false, 'trim' => false)
            )
            ->add('studentLdapOUPath', TextType::class,
                array('label' => 'Percorso delle unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppClientId', TextType::class, ['label' => "ClientId Google Apps (lasciare vuoto per disattivare)", 'required' => false])
            ->add('studentGoogleAppDomain', TextType::class, ['label' => "Dominio Google Apps", 'required' => false])
            ->add('studentGoogleAppClientSecret', TextType::class, ['label' => "Client Secret Google Apps", 'required' => false])
            ->add('studentGoogleAppAutoEmail', ChoiceType::class,
                array('label' => 'Comportamento per la creazione degli indirizzi email', 'required' => false,'choices'=>[
                    'Crea solo gli indirizzi email presenti nel provider'=>0,
                    'Crea gli indirizzi presenti nel provider e usa lo username se mancano'=>1,
                    'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider'=>2,
					'Crea gli indirizzi usando una sintassi specificata' => 4,
					'Crea gli indirizzi presenti nel provider oppure crea gli indirizzi usando una sintassi specificata' => 5,
                    'Non creare nulla, sospendi la creazione degli indirizzi email'=>3,
                ])
            )
			->add('studentGoogleAppAutoEmailStyle', ChoiceType::class,
				array('label' => 'Sintassi dell\'indirizzo email:', 'required' => false,
					  'choices' => ConfigurationManager::$usernameStyles
				))
            ->add('studentGoogleAppCreateGroup', CheckboxType::class,
                array('label' => 'Crea i gruppi di distribuzione', 'required' => false)
            )
            ->add('studentGoogleAppCreateProviderGroup', CheckboxType::class,
                array('label' => 'Crea il gruppo del provider', 'required' => false)
            )
            ->add('studentGoogleAppUseUserInProviderGroup', CheckboxType::class,
                array('label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false)
            )
            ->add('studentGoogleAppGroupPrefix', TextType::class,
                array('label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false)
            )
            ->add('studentGoogleAppGroupExtraEmail', TextareaType::class,
                array('label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false)
            )
            ->add('studentGoogleAppProviderGroupExtraEmail', TextareaType::class,
                array('label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false)
            )
            ->add('studentGoogleAppCreateOU', CheckboxType::class,
                array('label' => 'Crea le unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppOUPrefix', TextType::class,
                array('label' => 'Prefisso al nome delle unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppOUPath', TextType::class,
                array('label' => 'Percorso delle unità organizzative', 'required' => true, 'empty_data' => "/")
            );

        $builder
            ->add('teacherAutoCreateUsername', CheckboxType::class,
                array(
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false
                )
            )
			->add('teacherLdapAutoUsernameStyle', ChoiceType::class,
				array('label' => 'Sintassi dell\'username: (se diversa da quella generale)', 'required' => false,
					  'choices' => ConfigurationManager::$usernameStyles
				))
            ->add('teacherForceImportedPasswordInitial', CheckboxType::class,
                array('label' => "Imposta la password proveniente dal provider alla creazione dell'utente?", 'required' => false)
            )
            ->add('teacherForceImportedPassword', CheckboxType::class,
                array('label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false)
            )
			->add('teacherLdapExtendedUsernameSuffix', TextType::class,
				['label' => "Suffisso terminale/dominio per l'username esteso Active Directory (lasciare vuoto per usare il nome dominio generale)", 'required' => false]
			)
			->add('teacherLdapUseEmailForExtendedUsername', CheckboxType::class,
				array('label' => "Usa la prima parte dell'email come username esteso di Active Directory?", 'required' => false)
			)
            ->add('teacherLdapCreateOU', CheckboxType::class,
                array('label' => 'Crea l\'unità organizzativa', 'required' => false)
            )
            ->add('teacherLdapOUPrefix', TextType::class,
                array('label' => 'Nome dell\'unità organizzativa', 'required' => false, 'trim' => false, 'empty_data' => "Teachers")
            )
            ->add('teacherLdapOUPath', TextType::class,
                array('label' => 'Percorso delle unità organizzativa', 'required' => false)
            )
            ->add('teacherGoogleAppClientId', TextType::class, ['label' => "ClientId Google Apps (lasciare vuoto per disattivare)", 'required' => false])
            ->add('teacherGoogleAppDomain', TextType::class, ['label' => "Dominio Google Apps", 'required' => false])
            ->add('teacherGoogleAppClientSecret', TextType::class, ['label' => "Client Secret Google Apps", 'required' => false])
            ->add('teacherGoogleAppAutoEmail', ChoiceType::class,
                array('label' => 'Comportamento per la creazione degli indirizzi email', 'required' => false,'choices'=>[
                    'Crea solo gli indirizzi email presenti nel provider'=>0,
                    'Crea gli indirizzi presenti nel provider e usa lo username se mancano'=>1,
                    'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider'=>2,
                    'Crea gli indirizzi usando una sintassi specificata' => 4,
                    'Crea gli indirizzi presenti nel provider oppure crea gli indirizzi usando una sintassi specificata' => 5,
                    'Non creare nulla, sospendi la creazione degli indirizzi email'=>3,
                ])
            )
			->add('teacherGoogleAppAutoEmailStyle', ChoiceType::class,
				array('label' => 'Sintassi dell\'indirizzo email:', 'required' => false,
					  'choices' => ConfigurationManager::$usernameStyles
				))
            ->add('teacherGoogleAppCreateGroup', CheckboxType::class,
                array('label' => 'Crea i gruppi di distribuzione', 'required' => false)
            )
            ->add('teacherGoogleAppCreateProviderGroup', CheckboxType::class,
                array('label' => 'Crea il gruppo del provider', 'required' => false)
            )
            ->add('teacherGoogleAppUseUserInProviderGroup', CheckboxType::class,
                array('label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false)
            )
            ->add('teacherGoogleAppGroupPrefix', TextType::class,
                array('label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false)
            )
            ->add('teacherGoogleAppGroupExtraEmail', TextareaType::class,
                array('label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false)
            )
            ->add('teacherGoogleAppProviderGroupExtraEmail', TextareaType::class,
                array('label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false)
            )
            ->add('teacherGoogleAppCreateOU', CheckboxType::class,
                array('label' => 'Crea l\'unità organizzativa', 'required' => false)
            )
            ->add('teacherGoogleAppOUPrefix', TextType::class,
                array('label' => 'Nome dell\'unità organizzativa', 'required' => false, 'empty_data' => "Teachers")
            )
            ->add('teacherGoogleAppOUPath', TextType::class,
                array('label' => 'Percorso delle unità organizzativa', 'required' => true, 'empty_data' => "/")
            );

        $builder
            ->add('internetTeachersControl', CheckboxType::class,
                array('label' => 'Gli insegnanti possono controllare l\'accesso ad internet degli studenti?', 'required' => false)
            )
            ->add('internetOpenAccessRange', TextareaType::class,
                array('label' => 'Orari in cui internet è aperto per tutti gli studenti:', 'required' => false)
            );

        $pageSizes = [];
        foreach (CPDF::$PAPER_SIZES as $name => $size) {
            $pageSizes[$name] = $name;
        }

        $builder
            ->add('pdfHeader', CKEditorType::class, ['label' => 'Testata nelle comunicazioni stampate', 'required' => false])
            ->add('pdfHeaderHeight', IntegerType::class, ['label' => 'Altezza in px della testata', 'required' => FALSE])
            ->add('pdfFooter', CKEditorType::class, ['label' => 'Fondo pagina nelle comunicazioni stampate', 'required' => false])
            ->add('pdfFooterHeight', IntegerType::class, ['label' => 'Altezza in px del fondo pagina', 'required' => FALSE])
            ->add('pdfStudentBadge', CKEditorType::class, ['label' => 'Testo badge studenti', 'required' => false])
            ->add('pdfStudentBadgePageSize', ChoiceType::class, ['label' => "Tipologia di carta per badge studenti", 'choices' => $pageSizes, 'preferred_choices' => ['a4']])
            ->add('pdfStudentBadgePageLandscape', CheckboxType::class, ['label' => "Orientamento Orizzontale della carta per badge studenti", 'required' => FALSE])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults([
        'filterProviderData'=>[]
    ]);
}
    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'App\Entity\Provider'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_provider';
    }

    /**
     * @param mixed $filterProviderData
     */
    public function setFilterProviderData($filterProviderData) {
        $this->filterProviderData = $filterProviderData;
    }

    protected function generateFilterSelect() {

        $options = [];
        foreach( $this->filterProviderData as $id => $data) {
            $options[$data['name']]=$id;
        }

        return $options;

    }

    protected function generateFilterForm(FormBuilderInterface $builder) {

        $container = $builder->create("filterDataCont", null, ['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
        foreach($this->filterProviderData as $id => $data) {
            $form = $builder->create($id,null,['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
            foreach($data['ui'] as $uiId => $uiData) {
                if($uiData['type'] == PasswordType::class)
                    $form->add($uiId,$uiData['type'],['label' => $uiData['title'], 'always_empty' => false]);
                elseif($uiData['type'] == ChoiceType::class)
                    $form->add($uiId,$uiData['type'],['label' => $uiData['title'], 'choices' => $uiData['choices']]);
                else
                    $form->add($uiId,$uiData['type'],['label' => $uiData['title']]);
            }
            $container->add($form);
        }
        $builder->add($container);

    }
}
