<?php

namespace App\Form;

use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\Student;

class StudentType extends AbstractType
{

    protected $modelManager;

    function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $newEntity = $imported = $autoUsername = $autoEmail = false;
        if($entity instanceof Student) {
            if($entity->getId() == null) {
                $newEntity = TRUE;
            }

            $imported = ($entity->getIdOnProvider() != "");

            $provider = $entity->getProvider();
            if($provider instanceof Provider) {
                $autoUsername = $provider->getStudentAutoCreateUsername();
                $autoEmail = $provider->getStudentGoogleAppAutoEmail();
            }
        }

        $builder
                ->add('fiscalCode', TextType::class, array('label' => 'Codice Fiscale', 'required' => true))
                ->add('lastname', null, array('label' => 'Cognome', 'required' => true, 'disabled' => $imported)) //disabilito se importati da provider (id_on_provider non nullo)
                ->add('firstname', null, array('label' => 'Nome', 'required' => true, 'disabled' => $imported)) //disabilito se importati da provider
                ->add('username', null, array('label' => 'Username'.($autoUsername?" (Lasciare vuoto per generare automaticamente)":"")))  // , 'disabled' => $autoUsername
                ->add('email', null, array('label' => 'E-Mail'.($autoEmail?" (Lasciare vuoto per generare automaticamente)":""))) // , 'disabled' => $autoEmail
                ->add('provider', EntityType::class, ['label' => "Provider", 'class' => "App\Entity\Provider",
                                         'choice_label' => 'name', 'required' => true ]) //, 'disabled' => $imported
                ->add('additionalProviders', EntityType::class, [
                    'label' => "Provider Aggiuntivi",
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'class' => Provider::class
                ])
                ->add('memberOf', CollectionType::class, array(
                    'label' => 'Gruppi/Classi',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => false,
					'entry_type' => ModelHiddenType::class,
					'entry_options' => ['model_manager' => $this->modelManager, 'class' => Group::class]
                    )
                )
        ;
    }

    public function getName()
    {
        return 'app_studenttype';
    }

}
