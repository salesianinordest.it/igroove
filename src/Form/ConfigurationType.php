<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\Manager\ConfigurationManager;

class ConfigurationType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $letters = array();
        for ($i = 65; $i < 91; $i++) {
            $letters[chr($i)] = chr($i);
        }

        $builder
            ->add('onlyReception', CheckboxType::class, array(
                'label' => 'igroove è solo per la gestione degli ospiti e dei MAC address.',
                'required' => false))

            ->add('disableGuest', CheckboxType::class, array(
                'label' => 'disabilita la gestione degli ospiti',
                'required' => false))
            ->add('disableMikrotikBypass', CheckboxType::class, array(
                'label' => 'disabilita la gestione dei bypass hotspot',
                'required' => false))

            ->add('guestCanRegisterViaSms', CheckboxType::class, array('label' => 'Gli utenti ospiti possono registrarsi con un SMS.', 'required' => false))
            ->add('skebbyUsername', TextType::class, array('label' => 'Username:', 'required' => false, 'trim' => false))
            ->add('skebbyPassword', PasswordType::class, array('label' => 'Password:', 'required' => false, 'trim' => false))

            ->add('userCanChangeOwnPassword', CheckboxType::class, array('label' => 'Gli utenti (studenti e docenti) possono cambiare la loro password.', 'required' => false))
            ->add('showBadge', CheckboxType::class, array('label' => 'Gli utenti (studenti e docenti) possono stampare il loro badge con username e password. [ver. beta]', 'required' => false))
            ->add('teacherCanResetPassword', CheckboxType::class, array('label' => 'I docenti possono cambiare la password degli studenti.', 'required' => false))
            ->add('teacherCanSeeList', CheckboxType::class, array('label' => 'I docenti possono vedere i dati di accesso degli studenti.', 'required' => false))
            ->add('teacherCanEnablePersonalDevice', CheckboxType::class, array('label' => 'I docenti possono abilitare i dispositivi personali dei ragazzi.', 'required' => false))
            ->add('active_directory_account_suffix', TextType::class, array('label' => 'Nome corto del dominio:', 'required' => false))
            ->add('active_directory_base_dn', TextType::class, array('label' => 'LDAP Base DN:', 'required' => false))
            ->add('active_directory_domain_controller', TextType::class, array('label' => 'Indirizzo IP del domain controller: (È possibile inserirne più di uno, separato da virgola)', 'required' => false))
            ->add('active_directory_admin_username', TextType::class, array('label' => 'Username amministratore:', 'required' => false))
            ->add('active_directory_admin_password', PasswordType::class, array('label' => 'Password: (vuoto per non cambiare)', 'required' => false, 'always_empty' => false, 'attr' => array("autocomplete" => "off")))
            ->add('active_directory_use_ssl', CheckboxType::class, array('label' => 'Il server è abilitato alla scrittura (SSL).', 'required' => false))
            ->add('active_directory_sync_suffix', TextType::class, array('label' => 'Carattere finale di sincronizzazione (aggiungere per utilizzare più di un iGroove sullo stesso AD):', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_group_prefix', TextType::class, array('label' => 'Carattere iniziale dei gruppi creati:', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_teacher_group_prefix', TextType::class, array('label' => 'Carattere iniziale dei gruppi docenti creati: (vuoto per non creare)', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_provider_group_with_users', CheckboxType::class, array('label' => 'Inserisci gli utenti al posto dei gruppi nel gruppo provider studenti', 'required' => false))
            ->add('active_directory_generated_teacher_provider_group_with_users', CheckboxType::class, array('label' => 'Inserisci gli utenti al posto dei gruppi nel gruppo provider docenti', 'required' => false))
            ->add('active_directory_home_folder', TextType::class, array('label' => 'Home directory degli utenti:', 'required' => false))
            ->add('active_directory_home_drive',  ChoiceType::class, array('label' => 'Lettera del disco da mappare con la home directory:', 'required' => false, 'choices' => $letters))
            ->add('active_directory_password_min_char', IntegerType::class, array('label' => 'Numero minimo dei caratteri:', 'required' => false))
            ->add('active_directory_password_complexity', CheckboxType::class, array('label' => 'Necessaria password complessa.', 'required' => false))
            ->add('active_directory_username_style', ChoiceType::class, array('label' => 'Sintassi dei nomi utenti creati:', 'required' => false,
                'choices' => ConfigurationManager::$usernameStyles
            ))
            ->add('active_directory_email_for_error', TextType::class, array('label' => 'Indirizzo email per la notifica degli errori:', 'required' => false))
            ->add('email_length_limit', TextType::class, array('label' => 'Limite caratteri email (lasciare vuoto per mantenere 20 come l\'username, 0 per non limitare)', 'required' => false))

            ->add('wireless_ssid', TextType::class, array('label' => 'SSID rete guest:', 'required' => false))
            ->add('wireless_guest_network_type', TextType::class, array('label' => 'Tipo di rete guest:', 'required' => false))
            ->add('wireless_guest_network_rules', null, array('label' => 'Regolamento rete guest:', 'required' => false))
//            ->add('wireless_unifi_controller', TextType::class, array('label' => 'Indirizzo IP:', 'required' => false))
//            ->add('wireless_unifi_username', TextType::class, array('label' => 'Username:', 'required' => false))
//            ->add('wireless_unifi_password', PasswordType::class, array('label' => 'Password:', 'required' => false, 'always_empty' => false, 'attr' => array("autocomplete" => "off")))//           ->add('wireless_unifi_kick_expired', CheckboxType::class, array('label' => 'Scollega dal wifi gli studenti ai quali è scaduto il tempo. (Beta)', 'required' => false))
//            ->add('wireless_unifi_kick_expired_networks', TextType::class, array('label' => 'Nome delle reti dalle quali scollegare i ragazzi (separate da virgola, oppure lasciare vuoto per tutte)', 'required' => false))

            ->add('coa_endpoint', TextType::class, array('label' => 'Endpoint del server coa killer:', 'required' => false))
            ->add('coa_pattern', TextType::class, array('label' => 'Pattern di ricerca degli utenti (esempio: username@%):', 'required' => false))
            ->add('coa_password', PasswordType::class, array('label' => 'Password di autorizzazione:', 'required' => false))



            //            ->add('freeradius_realm', TextType::class, array('label' => 'Realm per gli studenti', 'required' => false))
            ->add('guest_realm', TextType::class, array('label' => 'Realm per gli ospiti', 'required' => false))

//            ->add('showStats', CheckboxType::class, array('label' => 'Visualizza statistiche', 'required' => false))
//            ->add('wirelessHotspotServer', TextType::class, array('label' => 'Nome del server hotspot Mikrotik per gli utenti wireless', 'required' => false))

            ->add('save', SubmitType::class, array('label' => 'Cambia la configurazione di igroove'));;
    }

    public function getName()
    {
        return 'configuration';
    }


}

