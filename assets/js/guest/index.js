import * as $ from 'jquery';

export default (function () {
    var sendToEmailModal = $("#sendToEmailModal");
    var sendToEmailField = $("#sendToEmailField");

    $(".sendCredentialToEmail").click(function () {
        var $bt = $(this);
        if ($bt.data("userid") === undefined || $bt.data("userid") === "" || $bt.data("userfullname") === undefined || $bt.data("userfullname") === "") {
            return;
        }

        sendToEmailModal.data("userid", $bt.data("userid"));
        var modalContent = sendToEmailModal.children(".modal-dialog").children(".modal-content");
        modalContent.children(".modal-body").children('.userFullName').text($bt.data("userfullname"));
        modalContent.children(".modal-body").children('.status').removeClass('alert').removeClass("alert-danger").removeClass("alert-success").text("");
        $("#sendToEmailField").val("");
        sendToEmailModal.modal("show");
    });

    $("#sendToEmailBt").click(function () {
        if (sendToEmailModal.data("userid") === undefined || sendToEmailModal.data("userid") === "") {
            return;
        }

        var modalContent = sendToEmailModal.children(".modal-dialog").children(".modal-content");
        var statusArea = modalContent.children(".modal-body").children('.status');
        statusArea.removeClass('alert').removeClass("alert-danger").removeClass("alert-success").text("");

        if (sendToEmailField.val() === "") {
            statusArea.addClass("alert").addClass("alert-danger").text("E-Mail non valida");
            return;
        }

        console.log(sendToEmailModal.data("userid"));
        "/guest/{id}/send_credential_to_email"
        /*
        $.get("{{ path("guest_send_credential_to_email",{'id': '%id'}) }}".replace("%25id", sendToEmailModal.data("userid")), {'emailTo': sendToEmailField.val()}, function (data) {
            if (data.success !== true) {
                statusArea.addClass("alert").addClass("alert-danger").html('Errore durante l\'elaborazione della richiesta:<br />' + data.error);
                return;
            }

            statusArea.addClass("alert").addClass("alert-success").text('Email inviata correttamente');
            sendToEmailField.val("");
        }, "json"
    ).
        fail(function (event, jqXHR) {
            statusArea.addClass("alert").addClass("alert-danger").text('Errore durante l\'elaborazione della richiesta');
        });
    });
    */
    })
})
