#!/bin/sh

if [ "$#" -ne 1 ]; then
    echo "load-senvironment.sh (nome)"
fi
nome=$1

echo [1/5] Caricamento .env.local
cp ../configurazioni/$nome/.env.local .env.$nome
cp ../configurazioni/$nome/igroove.sql .
cp -r ../configurazioni/$nome/etc_ldap .
grep ldap_ .env.$nome > .env.local
grep ROLE_ .env.$nome >> .env.local


echo [2/5] Caricamento igroove.sql
vagrant ssh-config > vagrant-ssh;

ssh -F vagrant-ssh linux  "mysql -u root -proot -e 'CREATE DATABASE IF NOT EXISTS igroove'";
ssh -F vagrant-ssh linux "mysql -u root -proot igroove < /vagrant/igroove.sql;exit"
ssh -F vagrant-ssh linux "php /vagrant/bin/console doctrine:schema:update --force";


echo [3/5] Caricamento configurazione ldaps
ssh -F vagrant-ssh linux "sudo cp -r /vagrant/etc_ldap/* /etc/ldap";


echo [4/5] Rimozione file di supporto
rm vagrant-ssh
rm igroove.sql
rm -rf etc_ldap
rm .env.$nome


echo [5/5] Rimozione logs e cache
rm -rf app/cache/*
rm -rf app/logs/*

