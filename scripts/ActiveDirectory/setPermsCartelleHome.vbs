#	User Home Directory Permissions - jocha.se 2013-01-15
#
#	Creates a HomeDirectory for users who are missing one.
#	Verifies they have Modify permissions, if they have Full it replaces with Modify.

# Loading modules
Import-Module ActiveDirectory

$DC = "DC01.DOMAIN.LOCAL"
$OU = "OU=Users,DC=DOMAIN,DC=LOCAL"

$Content = (Get-ADUser -server $Dc -filter * -Properties * -SearchBase $OU | select SamAccountName, HomeDirectory)

FOREACH ($ID in $Content) {
    $User = $ID.SamAccountName
    $Folder = $ID.HomeDirectory
    # If the user does not have a value for HomeDirectory it skips.
    If ($Folder) {
        # If the HomeDirectory does not exist its created.
        If ((Test-Path $Folder) -ne $true) {
            New-Item -ItemType directory -Path $Folder
            Write-Host icacls $Folder /grant $User`:`(OI`)`(CI`)M
            }
        # Checking if user has Full permissions on their folder.
        $Icacls = icacls $Folder
        $Match = "*" + $User + ":(F)*"
        $IcaclsResult = $Icacls -like $Match
        If ($IcaclsResult) {
            Write-Host $User " HomeDirectory has incorrect permissions. Resetting..."
            Write-Host icacls $Folder /remove:g $User
            Write-Host icacls $Folder /grant $User`:`(OI`)`(CI`)M
        }
    }
}
