Dim adoConnection, strBase, strFilter, strAttributes, strQuery, adoRecordset
Dim strDate, strUserName, dtmCreated, intCount, strDNSDomain



if ((WScript.Arguments.Count<2) or (WScript.Arguments.Count>3)) then
    WScript.Echo "Usage: deleteOldUsers.vbs {before|after} {anno} [force]"
    WScript.Quit
end if

strDate = WScript.Arguments.Item(1) & "0101000000.0Z"
condition=WScript.Arguments.Item(0)

force=0
if (WScript.Arguments.Count=3) then
   if (WScript.Arguments.Item(2)="force") then
   force=1
   end if
end if







Set adoConnection = CreateObject("ADODB.Connection")
adoConnection.Provider = "ADsDSOObject"
adoConnection.Open "Active Directory Provider"
Set adoRecordset = CreateObject("ADODB.Recordset")
adoRecordset.ActiveConnection = adoConnection

adoRecordset.Properties("Page Size")   = 1000

Set objRootDSE = GetObject("LDAP://RootDSE")
strDNSDomain = objRootDSE.Get("DefaultNamingContext")
strBase = "<LDAP://CN=Users," & strDNSDomain & ">"




if (condition="before") then
strFilter = "(&(objectCategory=person)(objectClass=user)" _
    & "(whenCreated<=" & strDate & ")(description=Creato da igroove))"
else
strFilter = "(&(objectCategory=person)(objectClass=user)" _
    & "(whenCreated>=" & strDate & ")(description=Creato da igroove))"
end if


strAttributes = "sAMAccountName,whenCreated"

strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"


adoRecordset.Source = strQuery
adoRecordset.Open
intCount = 0
Do Until adoRecordset.EOF
    strUserName = adoRecordset.Fields("sAMAccountName").Value
    dtmCreated = adoRecordset.Fields("whenCreated").Value
    intCount = intCount + 1
    adoRecordset.MoveNext
Loop

adoRecordset.Close
adoConnection.Close

WScript.Echo "Number of accounts created " & condition & " " & strDate & ": " & CStr(intCount)

