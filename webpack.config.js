var Encore = require('@symfony/webpack-encore');
var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

Encore
    .setOutputPath('public/assets')
    .setPublicPath('/assets')
    .addEntry('app', './assets/js/app.js')
    .enableSassLoader() // allow sass/scss files to be processed
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild() // empty the outputPath dir before each build
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default']
    }).addPlugin(
        new SWPrecacheWebpackPlugin(
        {
            cacheId: 'igroove',
            dontCacheBustUrlsMatching: /\.\w{8}\./,
            filename: 'service-worker.js',
            minify: true,
            navigateFallback: 'index.html',
            staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
        })
    )
    .enableBuildNotifications() // show OS notifications when builds finish/fail
    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();


