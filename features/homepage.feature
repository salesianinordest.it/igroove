# homepage.feature
Feature: User homepage
  As a registered user
  I see certain links based on my role

  Scenario: Login redirect
    When I am on the homepage
    Then I should be on "/login"


  Scenario: I belong to Domain Admins, Domain Users and Administrators groups
    Given I am on the homepage
    When I am logged as user "administrator" with password "2wsx-1qaz"
    Then I should be on "/"



