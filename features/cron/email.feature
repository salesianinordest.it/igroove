# cron.feature
Feature: php bin/console cron
  As crontab daily job
  I syncronize users, email and a lot of datas

  Background:
    Given all providers are disabled
    And provider "Studenti-csv" is correctly present, setup and active

    #  'Crea solo gli indirizzi email presenti nel provider'=>0,
    #  'Crea gli indirizzi presenti nel provider e usa lo username se mancano'=>1,
    #  'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider'=>2,
    #   'Non creare nulla, sospendi la creazione degli indirizzi email'=>3,


  Scenario: Run cron -  'Crea solo gli indirizzi email presenti nel provider'=>0,
    Given I hardly setup the provider "Studenti-csv" with these parameters:
      | key                            | value        |
      | studentGoogleAppAutoEmail      | 0            |
      | studentGoogleAppAutoEmailStyle | COGNOME.NOME |
    When I am the system
    And I run the command "cron"
    Then I should found in entity "Student" these records:
      | username           | email                             |
      | pippo.bianchi      | pippo.bianchi@mestre.network      |
      | paperino.verdi     | pap68@hotmail.com                 |
      | mario.rossi        | m.rossi@libero.it                 |
      | gianfranco.dilivio | gianfranco.dilivio@mestre.network |
      | barbara.ferrari    |                                   |


  Scenario: Run cron - 'Crea gli indirizzi presenti nel provider e usa lo username se mancano'=>1,
    Given I hardly setup the provider "Studenti-csv" with these parameters:
      | key                            | value        |
      | studentGoogleAppAutoEmail      | 1            |
      | studentGoogleAppAutoEmailStyle | COGNOME.NOME |
    When I am the system
    And I run the command "cron"
    Then I should found in entity "Student" these records:
      | username           | email                             |
      | pippo.bianchi      | pippo.bianchi@mestre.network      |
      | paperino.verdi     | pap68@hotmail.com                 |
      | mario.rossi        | m.rossi@libero.it                 |
      | gianfranco.dilivio | gianfranco.dilivio@mestre.network |
      | barbara.ferrari    | barbara.ferrari@mestre.network    |


  Scenario: Run cron - 'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider'=>2,
    Given I hardly setup the provider "Studenti-csv" with these parameters:
      | key                            | value        |
      | studentGoogleAppAutoEmail      | 2            |
      | studentGoogleAppAutoEmailStyle | COGNOME.NOME |
    When I am the system
    And I run the command "cron"
    Then I should found in entity "Student" these records:
      | username           | email                             |
      | pippo.bianchi      | pippo.bianchi@mestre.network      |
      | paperino.verdi     | paperino.verdi@mestre.network     |
      | mario.rossi        | mario.ross@mestre.network         |
      | gianfranco.dilivio | gianfranco.dilivio@mestre.network |
      | barbara.ferrari    | barbara.ferrari@mestre.network    |


  Scenario: Run cron - 'Non creare nulla, sospendi la creazione degli indirizzi email'=>3,
    Given I hardly setup the provider "Studenti-csv" with these parameters:
      | key                            | value        |
      | studentGoogleAppAutoEmail      | 3            |
      | studentGoogleAppAutoEmailStyle | COGNOME.NOME |
    When I am the system
    And I run the command "cron"
    Then I should found in entity "Student" these records:
      | username           | email                             |
      | pippo.bianchi      | pippo.bianchi@mestre.network      |
      | paperino.verdi     | pap68@hotmail.com                 |
      | mario.rossi        | m.rossi@libero.it                 |
      | gianfranco.dilivio | gianfranco.dilivio@mestre.network |
      | barbara.ferrari    |                                   |



