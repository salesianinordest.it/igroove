<?php

$serviceKey = "lkdjfgsh7456";

/* MEMO
 * Su windowd2012 R2
 * Controllare che ci siano i driver odbc a 64 bit.
 * Se non ci sono installare da https://www.microsoft.com/en-us/download/confirmation.aspx?id=13255
 * Creare un oggetto odbc a 64bit di sistema con nome segremat4igroove
 * che puntino al file anagrafe.mdb
 *
 */


/*
 * Versione 5.0.0
 */

$key = $_REQUEST['key'];

if ($key != md5($serviceKey . date('Ymd'))) {
    echo json_encode('1.no valid key');
    die;
}


include_once 'lib/adodb5/adodb.inc.php';
$db= newAdoConnection('access');
$db->debug = true;
$db->connect('segremat4igroove');

$sql = "SELECT Anagrafica.CodiceAlunno, 
    Anagrafica.Nome, Anagrafica.Cognome, Anagrafica.MailStudente, 
    Anagrafica.CF_Alunno, Anagrafica.DataNascita, Anagrafica.Classe, 
    Anagrafica.Settore, Anagrafica.SettoreClasse, Anagrafica.MailPadre, Anagrafica.MailMadre, Anagrafica.MailTutore
    FROM Anagrafica 
    WHERE Anagrafica.TipoNominativo=1
    ORDER BY Anagrafica.Cognome ASC, Anagrafica.Nome ASC;";
$recordSet = $db->execute($sql);
$elenco = array();
while (!$recordSet->EOF) {
    if (is_null($recordSet->fields[4])) {
        $recordSet->MoveNext();
        continue;
    }
    $id=@utf8_encode(trim($recordSet->fields[0]));
    $CF_Alunno = @utf8_encode(trim($recordSet->fields[4]));

    if (strlen($CF_Alunno) == 0) {
        $recordSet->MoveNext();
        continue;
    }
    $CodiceAlunno = utf8_encode(trim($recordSet->fields[0]));
    $Nome = $recordSet->fields[1];
    $Cognome = $recordSet->fields[2];
    $MailStudente = $recordSet->fields[3];
    $CF_Alunno = utf8_encode($recordSet->fields[4]);
    $DataNascita = $recordSet->fields[5];
    $Classe = $recordSet->fields[6];
    $Settore = $recordSet->fields[7];
    $SettoreClasse = $recordSet->fields[8];


    $genitori = array(
        array('email' => @utf8_encode(trim(strtolower($recordSet->fields[9])))),
        array('email' => @utf8_encode(trim(strtolower($recordSet->fields[10])))),
        array('email' => @utf8_encode(trim(strtolower($recordSet->fields[11]))))
    );

    $remove = array(
        '^',
        ',',
        '.',
        ':',
        '/',
        '\\',
        ',',
        '=',
        '+',
        '<',
        '>',
        ';',
        '"',
        '#',
        "'",
        '(',
        ')',
        "'",
        "\x00",
        '?',
        '.',
        '-',
        '!'
    );
    $SettoreClasse = str_replace($remove, '', $SettoreClasse);


    $elenco[$CF_Alunno] = array(
        'id'=> $id,
        'cf' => $CF_Alunno,
        'firstname' => utf8_encode(trim(ucwords(strtolower($Nome)))),
        'lastname' => utf8_encode(trim(ucwords(strtolower($Cognome)))),
        'email' => @utf8_encode(trim(strtolower($MailStudente))),
        'group' => utf8_encode(trim($SettoreClasse)),
        'genitori' => $genitori
    );

    $recordSet->MoveNext();
}


$sql = "SELECT
    Anagrafica.CF_Alunno
    FROM Anagrafica 
    WHERE Anagrafica.TipoNominativo=1 AND (Anagrafica.MailStudente='' OR Anagrafica.MailStudente IS NULL)";
$recordSet = $db->Execute($sql);
while (!$recordSet->EOF) {
    if (is_null($recordSet->fields[0])) {
        $recordSet->MoveNext();
        continue;
    }
    $CF_Alunno = @utf8_encode($recordSet->fields[0]);
    if (strlen($CF_Alunno) == 0) {
        $recordSet->MoveNext();
        continue;
    }
    $elenco[$CF_Alunno]['email'] = '';
    $recordSet->MoveNext();
}
$recordSet->Close();
$db->Close();
echo json_encode($elenco);
